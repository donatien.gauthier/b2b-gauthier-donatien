# TP1 Réseaux - Mise en jambes

## I. Exploration locale en solo

### 1. Affichage d'informations sur la pile TCP/IP locale

*  Affichez les infos des cartes réseau de votre PC:
    ```ipconfig```
```Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Adresse IPv6 de liaison locale. . . . .: fe80::3c5c:c2c1:3b9:b6cf%13
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.3.243
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
``` 
```Carte Ethernet Ethernet :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . : home
```

* Affichez votre gateway:
    ```ipconfig```
    passerelle: 10.33.3.253
    
    ```panneau de configuration```>```réseau et internet```>```centre réseau et partage```:```wifi```>```détails```
![](/image/infocarteIPTPréseaux.png)


* Trouvez comment afficher les informations sur une carte IP (change selon l'OS):

```ipconfig ```
```Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Adresse IPv6 de liaison locale. . . . .: fe80::3c5c:c2c1:3b9:b6cf%13
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.3.243
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
``` 

la gateway est un système matériel et logiciel permettant de faire la liaison entre deux réseaux

### 2. Modifications des informations

#### A. Modification d'adresse IP (part 1)

```panneau de configuration```>```réseau et internet```>```centre réseau et partage```:```connexion réseau```:```propriétés```:```modifier adresse IP```
![](/image/ModifiéadresseIP.png)

parce que

#### B. Table ARP

Explorations de la table ARP:
```arp -a```
![](/image/repérer adresse passerelle.png)
on la repère en utilisant l'adresse IP de la passerelle et on regarde l'adresse max associé à cette adresse IP

ping : 
10.33.3.109 >>> 38-f9-d3-2f-c2-79
10.33.3.112 >>> 3c-06-30-2d-48-0d
10.33.3.130 >>> 18-1d-ea-b4-8c-82
![](/image/repéreradressepasserelle.png)


#### C. nmap

![](/image/nmap.png)

#### D. Modification d'adresse IP (part 2)

![](/image/zenmapcommande.png)

## II. Exploration locale en duo

### 1. Prérequis

### 2. Câblage

### 3. Modification d'adresse IP

![](/image/IPethernet.png)
![](/image/pingethernet.png)
![](/image/tableARPethernet.png)

### 4. Utilisation d'un des deux comme gateway

![](/image/pingpartagedeco.png)
![](/image/tracert.png)

### 5. Petit chat privé

```
PS C:\netcat-1.11> .\nc.exe -l -p 8888
ssalsa
fdddddd
yo

d
dd
salut
salut mec
ca va ou quoi
yo gang
tu as rien vu leo
```
```
nc.exe 192.168.1.1 8888
yo gang
salut
yoo
ezee
sdvvdsv
efkejkhf
fefe
```

### 6. Firewall


```
PS C:\WINDOWS\system32> netsh advfirewall firewall add rule name="ICMP Allow incoming V4 echo request" protocol=icmpv4:8,any dir=in action=allow
Ok.

PS C:\WINDOWS\system32> ping 192.168.1.2

Envoi d’une requête 'Ping'  192.168.1.2 avec 32 octets de données :
Réponse de 192.168.1.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.1.2 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.1.2 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.1.2 : octets=32 temps=3 ms TTL=128

Statistiques Ping pour 192.168.1.2:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 3ms, Moyenne = 1ms 
```

![](/image/porttrafic.png)

```
PS C:\netcat-1.11> .\nc.exe 192.168.1.2 8888
éhrezufz
k
```

## III. Manipulations d'autres outils/protocoles côté client

### 1. DHCP

Avec ipconfig /all j'obtiens : 
    Bail obtenu. . . . . . . . . . . . . . : vendredi 17 septembre 2021 15:36:22
    Bail expirant. . . . . . . . . . . . . : vendredi 17 septembre 2021 17:36:23
    Serveur DHCP . . . . . . . . . . . . . : 10.33.3.254


### Dns
Avec ipconfig /all : 
Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                       10.33.10.148
                                       10.33.10.155



Je réalise la commande 
nslookup
 avec : 
ynov.com = 92.243.16.143
google.com = 216.58.213.78

L'ip du serveur auquel nous avons fait les requêtes : 10.33.10.2

Maintenant on inverse commande : 
78.74.21.21 = host-78-74-21-21.homerun.telia.com

92.146.54.88 = apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr


## IV. Wireshark

![](/image/wireshark.png)
![](/image/wireshark2.png)

