# TP2 : On va router des trucs

## I. ARP

### 1. Echange ARP

```
[dona@node1 ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=1.10 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=1.36 ms
64 bytes from 10.2.1.12: icmp_seq=3 ttl=64 time=1.22 ms
64 bytes from 10.2.1.12: icmp_seq=4 ttl=64 time=1.24 ms
64 bytes from 10.2.1.12: icmp_seq=5 ttl=64 time=1.33 ms
64 bytes from 10.2.1.12: icmp_seq=6 ttl=64 time=1.37 ms
64 bytes from 10.2.1.12: icmp_seq=7 ttl=64 time=1.33 ms
64 bytes from 10.2.1.12: icmp_seq=8 ttl=64 time=1.30 ms
64 bytes from 10.2.1.12: icmp_seq=9 ttl=64 time=0.888 ms
64 bytes from 10.2.1.12: icmp_seq=10 ttl=64 time=1.10 ms
64 bytes from 10.2.1.12: icmp_seq=11 ttl=64 time=1.22 ms
^C
--- 10.2.1.12 ping statistics ---
11 packets transmitted, 11 received, 0% packet loss, time 10017ms
rtt min/avg/max/mdev = 0.888/1.222/1.369/0.146 ms
[dona@node1 ~]$ ip n s
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:52 REACHABLE
10.2.1.12 dev enp0s8 lladdr 08:00:27:01:4d:f4 REACHABLE
```
```
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:01:4d:f4
```


```
[dona@node2 ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=0.699 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=1.14 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=64 time=1.32 ms
^C
--- 10.2.1.11 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2020ms
rtt min/avg/max/mdev = 0.699/1.054/1.321/0.264 ms
[dona@node2 ~]$ ip n s
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:52 REACHABLE
10.2.1.11 dev enp0s8 lladdr 08:00:27:7e:02:0c REACHABLE
```
```
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:7e:02:0c
```

### 2. Analyse de trames

![](/image/wireshark3.png)

| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Requête ARP | x         | `PcsCompu_01:4d:f4`       | x              | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | x         | `PcsCompu_7e:02:0c`       | x              | `PcsCompu_01:4d:f4`        |

## II. Routage

### 1. Mise en place du routage

```
[dona@router ~]$ sudo firewall-cmd --list-all
[sudo] password for dona:
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8 enp0s9
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[dona@router ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s3 enp0s8 enp0s9
[dona@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```

```
[dona@marcel ~]$ sudo nano /etc/sysconfig/network-scripts/route-enp0s8
[dona@marcel ~]$ ip route show
10.2.2.0/24 dev enp0s8 proto kernel scope link src 10.2.2.12 metric 100
```
Faire la meme chose sur l'autre VM.
```
[dona@router ~]$ sudo sysctl -p /etc/sysctl.d/routeur.conf
net.ipv4.ip_forward = 1
```
```
[dona@marcel ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=63 time=1.85 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=63 time=2.34 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=63 time=2.21 ms
64 bytes from 10.2.1.11: icmp_seq=4 ttl=63 time=1.35 ms
64 bytes from 10.2.1.11: icmp_seq=5 ttl=63 time=1.01 ms
64 bytes from 10.2.1.11: icmp_seq=6 ttl=63 time=1.97 ms
64 bytes from 10.2.1.11: icmp_seq=7 ttl=63 time=0.965 ms
64 bytes from 10.2.1.11: icmp_seq=8 ttl=63 time=2.32 ms
^C
--- 10.2.1.11 ping statistics ---
8 packets transmitted, 8 received, 0% packet loss, time 7011ms
rtt min/avg/max/mdev = 0.965/1.752/2.341/0.532 ms
```

### 2. Analyse de trames

```
[dona@node1 ~]$ ip neigh show
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:13 REACHABLE
10.2.1.254 dev enp0s8 lladdr 08:00:27:a7:a5:36 STALE
```
On peut deduire que la machine a pu communiquer avec le routeur grace a la passerelle
```
[dona@router ~]$ ip neigh show
10.2.2.12 dev enp0s8 lladdr 08:00:27:66:ae:38 STALE
10.2.1.1 dev enp0s9 lladdr 0a:00:27:00:00:13 DELAY
10.2.1.11 dev enp0s9 lladdr 08:00:27:7e:02:0c STALE
```
Qui lui l'a transmit a marcel
```
[dona@marcel ~]$ ip neigh show
10.2.2.1 dev enp0s8 lladdr 0a:00:27:00:00:0e DELAY
10.2.2.254 dev enp0s8 lladdr 08:00:27:fb:a9:28 STALE
```
Et marcel l'a reçu grace a sa passerelle

| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            | Info                             |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|----------------------------------|
| 1     | Requête ARP | x         |`node1``PcsCompu_fb:a9:28` | x              | Broadcast `FF:FF:FF:FF:FF` |Who has 10.2.2.12? Tell 10.2.2.254|
| 2     | Réponse ARP | x         |`router``PcsCompu_66:ae:38`| x              | `node1` `PcsCompu_fb:a9:28`|10.2.2.12 is at 08:00:27:66:ae:38 |
| 1     | Requête ARP | x         |`router``PcsCompu_66:ae:38`| x              | `marcel``PcsCompu_fb:a9:28`|Who has 10.2.2.254? Tell 10.2.2.12|
| 2     | Réponse ARP | x         |`marcel``PcsCompu_fb:a9:28`| x              | `router``PcsCompu_66:ae:38`|10.2.2.254 is at 08:00:27:fb:a9:28|
| ...   | ...         | ...       | ...                       |                |                            |
| ?     | Ping        | ?         | ?                         | ?              | ?                          |
| ?     | Pong        | ?         | ?                         | ?              | ?                          |


### 3. Accès internet

``` [dona@node1 ~]$ sudo ip route add default via 10.2.1.254 dev enp0s8 ```

```
[dona@node1 ~]$ sudo nano /etc/sysconfig/network
[dona@node1 ~]$ cat /etc/sysconfig/network
# Created by anaconda
GATEWAY=10.2.1.254
```
```
[dona@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=22.5 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=20.9 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=114 time=20.2 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=114 time=114 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 20.208/44.305/113.647/40.043 ms
```





