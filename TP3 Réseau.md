# TP3 : Progressons vers le réseau d'infrastructure

## I. (mini)Architecture réseau


| Nom du réseau | Adresse du réseau | Masque           | Nombre de clients possibles | Adresse passerelle | [Adresse broadcast] |
|---------------|-------------------|------------------|-----------------------------|--------------------|---------------------|
| `client1`     | `10.3.1.128`      |`255.255.255.192` | 62                          | `10.3.3.190`        | `10.3.3.191`
| `server1`     | `10.3.1.0`        |`255.255.255.128` | 126                         | `10.3.1.126`       | `10.3.1.127` 
| `server2`     | `10.3.1.192`      |`255.255.255.240` | 14                          | `10.3.2.206`        | `10.3.2.207`    


| Nom machine  | Adresse de la machine dans le réseau `client1` | Adresse dans `server1` | Adresse dans `server2` | Adresse de passerelle |
|--------------|------------------------------------------------|------------------------|------------------------|-----------------------|
| `router.tp3` | `10.3.1.128/26`                                | `10.3.1.0/25`          | `10.3.1.192/28`        | Carte NAT             |
| ...          | ...                                            | ...                    | ...                    | `10.3.?.?/?`          |



## 2. Routeur

```
[dona@router ~]$ ip a
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:99:d5:77 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:fb:a9:28 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.126/25 brd 10.3.1.127 scope global noprefixroute enp0s8
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:a7:a5:36 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.206/28 brd 10.3.1.207 scope global noprefixroute enp0s9
5: enp0s10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:38:c8:57 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.190/26 brd 10.3.1.191 scope global noprefixroute enp0s10
```

# **II. Services d'infra**
## **1. Serveur DHCP**

**🖥️ VM `dhcp.client1.tp3`**
#### **🌞 Mettre en place une machine qui fera office de serveur DHCP dans le réseau `client1`. Elle devra :**

    ```
    [dona@dhcp ~]$ echo 'dhcp.client1.tp3' | sudo tee /etc/hostname
    dhcp.client1.tp3
    [dona@dhcp ~]$ hostname
    dhcp.client1.tp3
    ```
    ```
    [dona@dhcp ~]$ sudo dnf -y install dhcp-server
    [dona@dhcp ~]$ sudo firewall-cmd --zone=public --permanent --add-service=dhcp; sudo firewall-cmd --reload; sudo firewall-cmd --list-all
    success
    success
    public (active)
      target: default
      icmp-block-inversion: no
      interfaces: enp0s8
      sources:
      services: dhcp ssh
      ports:
      protocols:
      masquerade: no
      forward-ports:
      source-ports:
      icmp-blocks:
      rich rules:

    [dona@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
    #
    # DHCP Server Configuration file.
    #   see /usr/share/doc/dhcp-server/dhcpd.conf.example
    #   see dhcpd.conf(5) man page
    #
    # Bail de 24H, max 48H
    default-lease-time 86400;
    max-lease-time 172800;

    # Déclaration du réseau "client1" (10.3.0.0/26) + range
    subnet 10.3.0.0 netmask 255.255.255.192 {
            range                           10.3.0.10 10.3.0.50;
            option domain-name-servers      1.1.1.1;
            option routers                  10.3.0.62;
    }
    
    [dona@dhcp ~]$ sudo systemctl start dhcpd; sudo systemctl enable dhcpd
    Created symlink /etc/systemd/system/multi-user.target.wants/dhcpd.service → /usr/lib/systemd/system/dhcpd.service.
    ```

#### **🌞 Mettre en place un client dans le réseau `client1` :**
> **🖥️ VM `marcel.client1.tp3`**
- 
   ```
    [dona@marcel ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
    TYPE=Ethernet
    BOOTPROTO=dhcp
    DEFROUTE=yes
    NAME=enp0s8
    UUID=c077c523-fb65-4a2a-8998-c40b1466e030
    DEVICE=enp0s8
    ONBOOT=yes

    [dona@marcel ~]$ sudo dhclient
    [dona@marcel ~]$ sudo nmcli device show enp0s8
    [...]
    IP4.ADDRESS[1]:                         10.3.0.11/26
    IP4.GATEWAY:                            10.3.0.62
    IP4.DNS[1]:                             1.1.1.1
    [...]
    ```
    
#### **🌞 Depuis `marcel.client1.tp3` :**
    ```
    [dona@marcel ~]$ sudo dhclient -r; sudo dhclient -v
    Killed old client process
    [...]
    DHCPDISCOVER on enp0s8 to 255.255.255.255 port 67 interval 8 (xid=0x2379bb5a)
    DHCPREQUEST on enp0s8 to 255.255.255.255 port 67 (xid=0x2379bb5a)
    DHCPOFFER from 10.3.0.61
    DHCPACK from 10.3.0.61 (xid=0x2379bb5a)
    bound to 10.3.0.11 -- renewal in 42656 seconds.
    
    [dona@dhcp ~]$ sudo cat /var/lib/dhcpd/dhcpd.leases
    [...]
    lease 10.3.0.11 {
      starts 2 2021/09/28 00:01:51;
      ends 3 2021/09/29 00:01:51;
      cltt 2 2021/09/28 00:01:51;
      binding state active;
      next binding state free;
      rewind binding state free;
      hardware ethernet 08:00:27:46:38:22;
      uid "\001\010\000'F8\"";
      client-hostname "marcel";
    }
    [...]
    
    ## Route par défaut = router.tp3
    [dona@marcel ~]$ ip r s
    default via 10.3.0.62 dev enp0s8 proto dhcp metric 100
    10.3.0.0/26 dev enp0s8 proto kernel scope link src 10.3.0.11 metric 100

    [dona@marcel ~]$ ping 8.8.8.8
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
    64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=13.2 ms
    
    --- 8.8.8.8 ping statistics ---
    1 packets transmitted, 1 received, 0% packet loss, time 0ms
    rtt min/avg/max/mdev = 13.203/13.203/13.203/0.000 ms
    
    [dona@marcel ~]$ dig google.com
    ; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
    ;; global options: +cmd
    ;; Got answer:
    ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 50717
    ;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

    ;; OPT PSEUDOSECTION:
    ; EDNS: version: 0, flags:; udp: 1232
    ;; QUESTION SECTION:
    ;google.com.                    IN      A

    ;; ANSWER SECTION:
    google.com.             87      IN      A       216.58.209.238

    ;; Query time: 20 msec
    ;; SERVER: 1.1.1.1#53(1.1.1.1)
    ;; WHEN: Tue Sep 28 02:10:32 CEST 2021
    ;; MSG SIZE  rcvd: 55
    ```
    
    ```
    [dona@marcel ~]$ traceroute -4 8.8.8.8
    traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
     1  _gateway (10.3.0.62)  1.764 ms  1.730 ms  1.643 ms
     2  10.0.2.2 (10.0.2.2)  1.617 ms  1.469 ms  1.434 ms
     3  10.0.2.2 (10.0.2.2)  2.399 ms  2.232 ms  1.906 ms
    ```
    > **On voit (ligne1) que `marcel.client1.tp3` passe par `router.tp3` (10.3.0.62) pour sortir de son réseau**

## **2. Serveur DNS**

### **A. Our own DNS server**
**A la fin donc, votre serveur sera :**

- **Autoritatif pour les zones `server1.tp3` et `server2.tp3`**
- **Resolver local**
- **Forwarder (pour résoudre les noms publics)**

### **B. SETUP copain**

> **🖥️ VM `dns1.server1.tp3`**

#### **🌞 Mettre en place une machine qui fera office de serveur DNS**
    [dona@dns1 ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
    TYPE=Ethernet
    BOOTPROTO=static
    IPADDR=10.3.0.253
    NETMASK=255.255.255.128
    DEFROUTE=yes
    NAME=enp0s8
    DEVICE=enp0s8
    ONBOOT=yes
    GATEWAY=10.3.0.254
    DNS1=10.3.0.253
    
    [dona@dns1 ~]$ sudo nmcli device show enp0s8
    [...]
    IP4.ADDRESS[1]:                         10.3.0.253/25
    IP4.GATEWAY:                            10.3.0.254
    IP4.DNS[1]:                             10.3.0.253
    [...]
    
    [dona@dns1 ~]$ hostname
    dns1.server1.tp3
    ```
    [dona@dns1 ~]$ sudo cat /etc/resolv.conf
    # Generated by NetworkManager
    nameserver 1.1.1.1

    [dona@dns1 ~]$ dig google.com

    ; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
    ;; global options: +cmd
    ;; Got answer:
    ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 49706
    ;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

    ;; OPT PSEUDOSECTION:
    ; EDNS: version: 0, flags:; udp: 1232
    ;; QUESTION SECTION:
    ;google.com.                    IN      A

    ;; ANSWER SECTION:
    google.com.             198     IN      A       172.217.22.142

    ;; Query time: 19 msec
    ;; SERVER: 1.1.1.1#53(1.1.1.1)
    ;; WHEN: Tue Sep 28 03:12:45 CEST 2021
    ;; MSG SIZE  rcvd: 55
    ```
    > Résolution du nom google.com : 
    `google.com.             198     IN      A       172.217.22.142`

    ```
    [dona@dns1 ~]$ sudo dnf install -y bind bind-utils
    [dona@dns1 ~]$ sudo firewall-cmd --add-service=dns --permanent; sudo firewall-cmd --reload; sudo firewall-cmd --list-all
    success
    success
    public (active)
      target: default
      icmp-block-inversion: no
      interfaces: enp0s8
      sources:
      services: dns ssh
      ports:
      protocols:
      masquerade: no
      forward-ports:
      source-ports:
      icmp-blocks:
      rich rules:

    [dona@dns1 ~]$ sudo nano /etc/named.conf
    [dona@dns1 ~]$ sudo nano /var/named/client1.tp3.forward
    [dona@dns1 ~]$ sudo nano /var/named/server1.tp3.forward
    [dona@dns1 ~]$ sudo nano /var/named/server2.tp3.forward
    [dona@dns1 ~]$ sudo systemctl enable named; sudo systemctl start named
    ```

    ```
    // named.conf
    
    acl "allowed" {
            10.3.0.0/24;
            10.3.0.1/28;
    };

    options {
            listen-on port 53 { 127.0.0.1; 10.3.0.253; };
            listen-on-v6 port 53 { ::1; };

            directory       "/var/named";
            dump-file       "/var/named/data/cache_dump.db";
            statistics-file "/var/named/data/named_stats.txt";
            memstatistics-file "/var/named/data/named_mem_stats.txt";
            secroots-file   "/var/named/data/named.secroots";
            recursing-file  "/var/named/data/named.recursing";

            allow-query     { localhost; allowed; };
            recursion       yes;
            dnssec-enable   yes;
            dnssec-validation yes;

            managed-keys-directory "/var/named/dynamic";
            pid-file "/run/named/named.pid";
            session-keyfile "/run/named/session.key";
            include "/etc/crypto-policies/back-ends/bind.config";
    };

    logging {
            channel default_debug {
                    file "data/named.run";
                    severity dynamic;
            };
    };

    # Déclaration des zones
    zone "client1.tp3" IN {
            type master;
            file "/var/named/client1.tp3.forward";
            allow-update { none; };
    };

    zone "server1.tp3" IN {
            type master;
            file "/var/named/server1.tp3.forward";
            allow-update { none; };
    };

    zone "server2.tp3" IN {
            type master;
            file "/var/named/server2.tp3.forward";
            allow-update { none; };
    };

    include "/etc/named.rfc1912.zones";
    include "/etc/named.root.key";
    ```
    > Pour vérifier la conf de `named.conf`, la commande ne doit rien retourner s'il n'y a pas d'erreur de syntaxe
    ```
    [dona@dns1 ~]$ sudo named-checkconf
    ```

    - **Des fichiers de zone "forward"**
        - **`client1.tp3.forward` (juste pour dhcp qui a une IP fixe)**
        ```
        [dona@dns1 ~]$ sudo cat /var/named/client1.tp3.forward
        [sudo] password for dona:
        $TTL 86400
        @       IN SOA  dns1.server1.tp3. root.server1.tp3 (
                        2021062301   ; serial
                        21600        ; refresh
                        3600         ; retry
                        604800       ; expire
                        86400 )      ; minimum TTL

        ; Define nameservers
        @       IN NS dns1.server1.tp3.
        ; Clients record
        dhcp    IN A 10.3.0.61
        ```
        > Vérifier la conf de la zone `client1.tp3`
        ```
        [dona@dns1 ~]$ sudo named-checkzone client1.tp3 /var/named/client1.tp3.forward
        zone client1.tp3/IN: loaded serial 2021062301
        OK
        ```

        - **`server1.tp3.forward`**
        ```
        [dona@dns1 ~]$ sudo cat /var/named/server1.tp3.forward
        $TTL 86400
        @       IN SOA  dns1.server1.tp3. root.server1.tp3 (
                        2021062301   ; serial
                        21600        ; refresh
                        3600         ; retry
                        604800       ; expire
                        86400 )      ; minimum TTL

        ; Define nameservers
        @       IN NS   dns1.server1.tp3.
        ; DNS Server IP Adresses
        dns1    IN A    10.3.0.253
        ```
        > Vérifier la conf de la zone `server1.tp3`
        ```
        [dona@dns1 ~]$ sudo named-checkzone server1.tp3 /var/named/server1.tp3.forward
        zone server1.tp3/IN: loaded serial 2021062301
        OK
        ```

        - **`server2.tp3.forward` (j'ai rajouté web1 et nfs1 après comme je n'avais pas encore de machines dans server2)**
        ```
        $TTL 86400
        @       IN SOA  dns1.server1.tp3. root.server1.tp3 (
                        2021062301   ; serial
                        21600        ; refresh
                        3600         ; retry
                        604800       ; expire
                        86400 )      ; minimum TTL

        ; Define nameservers
        @       IN NS   dns1.server1.tp3.
        ; Clients records
        web1    IN A    10.3.1.2
        nfs1    IN A    10.3.1.3
        ```
        > Vérifier la conf de la zone `server2.tp3`
        ```
        [dona@dns1 ~]$ sudo named-checkzone server2.tp3 /var/named/server2.tp3.forward
        zone server2.tp3/IN: loaded serial 2021062301
        OK
        ```

#### **🌞 Tester le DNS depuis `marcel.client1.tp3`**
    ```
    [dona@marcel ~]$ cat /etc/resolv.conf
    # Generated by NetworkManager
    search client1.tp3
    nameserver 10.3.0.253 
    ```

    ```
    [dona@marcel ~]$ dig dhcp.client1.tp3 @10.3.0.253

    ; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> dhcp.client1.tp3 @10.3.0.253
    ;; global options: +cmd
    ;; Got answer:
    ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 8582
    ;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 2

    ;; OPT PSEUDOSECTION:
    ; EDNS: version: 0, flags:; udp: 1232
    ; COOKIE: 2c05506ff15b1d3eba098f086154c2d73eb0f919d3d93f0b (good)
    ;; QUESTION SECTION:
    ;dhcp.client1.tp3.              IN      A

    ;; ANSWER SECTION:
    dhcp.client1.tp3.       86400   IN      A       10.3.0.61

    ;; AUTHORITY SECTION:
    client1.tp3.            86400   IN      NS      dns1.server1.tp3.

    ;; ADDITIONAL SECTION:
    dns1.server1.tp3.       86400   IN      A       10.3.0.253

    ;; Query time: 3 msec
    ;; SERVER: 10.3.0.253#53(10.3.0.253)
    ;; WHEN: Wed Sep 29 21:47:33 CEST 2021
    ;; MSG SIZE  rcvd: 132
    ```
    
    - **dig `dns1.server1.tp3` :**
    ```
    [dona@marcel ~]$ dig dns1.server1.tp3 @10.3.0.253

    ; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> dns1.server1.tp3 @10.3.0.253
    ;; global options: +cmd
    ;; Got answer:
    ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 8928
    ;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 1

    ;; OPT PSEUDOSECTION:
    ; EDNS: version: 0, flags:; udp: 1232
    ; COOKIE: 2e6c0c6b9e658ac28b39fc956154c2c5d64e09c0ee1a4102 (good)
    ;; QUESTION SECTION:
    ;dns1.server1.tp3.              IN      A

    ;; ANSWER SECTION:
    dns1.server1.tp3.       86400   IN      A       10.3.0.253

    ;; AUTHORITY SECTION:
    server1.tp3.            86400   IN      NS      dns1.server1.tp3.

    ;; Query time: 1 msec
    ;; SERVER: 10.3.0.253#53(10.3.0.253)
    ;; WHEN: Wed Sep 29 21:47:15 CEST 2021
    ;; MSG SIZE  rcvd: 103
    ```
    

    ```
    ;; SERVER: 10.3.0.253#53(10.3.0.253)
    ```
     
    ```
    [dona@marcel ~]$ nslookup dhcp.client1.tp3
    Server:         10.3.0.253
    Address:        10.3.0.253#53

    Name:   dhcp.client1.tp3
    Address: 10.3.0.61
    --------------------------------------------
    [dona@marcel ~]$ nslookup dns1.server1.tp3
    Server:         10.3.0.253
    Address:        10.3.0.253#53

    Name:   dns1.server1.tp3
    Address: 10.3.0.253
    ```

#### **🌞 Configurez l'utilisation du serveur DNS sur TOUS vos noeuds**
    ```   
    [dona@dhcp ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s8 | grep DNS1
    DNS1=10.3.0.253

    [dona@dns1 ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s8 | grep DNS1
    DNS1=10.3.0.253
    ```

    ```
    [dona@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf | grep "option domain-name-servers"
        option domain-name-servers      10.3.0.253;
    [dona@dhcp ~]$ sudo systemctl restart dhcpd
    
    [dona@marcel ~]$ sudo dhclient -r; sudo dhclient
    [dona@marcel ~]$ sudo cat /etc/resolv.conf
    ; generated by /usr/sbin/dhclient-script
    nameserver 10.3.0.253 
    ```
## **3. Get deeper**

### **A. DNS forwarder**
#### **🌞 Affiner la configuration du DNS**

    ```
    [dona@dns1 ~]$ sudo cat /etc/named.conf | grep recursion
        recursion       yes;
    ```

#### **🌞 Test !**

    ```
    [dona@marcel ~]$ dig google.com

    ; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
    ;; global options: +cmd
    ;; Got answer:
    ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 47566
    ;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 4, ADDITIONAL: 9

    ;; OPT PSEUDOSECTION:
    ; EDNS: version: 0, flags:; udp: 1232
    ; COOKIE: 35278fae4720709d9ebc00106154efd4e13cf8b037c91443 (good)
    ;; QUESTION SECTION:
    ;google.com.                    IN      A

    ;; ANSWER SECTION:
    google.com.             300     IN      A       142.250.75.238

    ;; AUTHORITY SECTION:
    google.com.             172800  IN      NS      ns3.google.com.
    google.com.             172800  IN      NS      ns4.google.com.
    google.com.             172800  IN      NS      ns2.google.com.
    google.com.             172800  IN      NS      ns1.google.com.

    ;; ADDITIONAL SECTION:
    ns2.google.com.         172800  IN      A       216.239.34.10
    ns1.google.com.         172800  IN      A       216.239.32.10
    ns3.google.com.         172800  IN      A       216.239.36.10
    ns4.google.com.         172800  IN      A       216.239.38.10
    ns2.google.com.         172800  IN      AAAA    2001:4860:4802:34::a
    ns1.google.com.         172800  IN      AAAA    2001:4860:4802:32::a
    ns3.google.com.         172800  IN      AAAA    2001:4860:4802:36::a
    ns4.google.com.         172800  IN      AAAA    2001:4860:4802:38::a

    ;; Query time: 270 msec
    ;; SERVER: 10.3.0.253#53(10.3.0.253)
    ;; WHEN: Thu Sep 30 00:59:31 CEST 2021
    ;; MSG SIZE  rcvd: 331
    ```

### **B. On revient sur la conf du DHCP**
> **🖥️ VM `johnny.client1.tp3`**

#### **🌞 Affiner la configuration du DHCP**
    ```
    [dona@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf | grep "option domain-name-servers"
            option domain-name-servers      10.3.0.253;
    ```
```
[dona@patron ~]$ sudo dhclient 
PS C:\Users\dona> ssh 10.3.0.12
[dona@patron ~]$ echo 'johnny.client1.tp3' | sudo tee /etc/hostname; sudo reboot now

[dona@johnny ~]$ sudo dhclient -r; sudo dhclient -v
[sudo] password for dona:
Internet Systems Consortium DHCP Client 4.3.6
Copyright 2004-2017 Internet Systems Consortium.
All rights reserved.
For info, please visit https://www.isc.org/software/dhcp/

Listening on LPF/enp0s8/08:00:27:b2:f4:9a
Sending on   LPF/enp0s8/08:00:27:b2:f4:9a
Sending on   Socket/fallback
DHCPDISCOVER on enp0s8 to 255.255.255.255 port 67 interval 8 (xid=0x404db54)
DHCPDISCOVER on enp0s8 to 255.255.255.255 port 67 interval 10 (xid=0x404db54)
DHCPREQUEST on enp0s8 to 255.255.255.255 port 67 (xid=0x404db54)
DHCPOFFER from 10.3.0.61
DHCPACK from 10.3.0.61 (xid=0x404db54)
bound to 10.3.0.13 -- renewal in 36802 seconds.

[dona@johnny ~]$ ip r s
default via 10.3.0.62 dev enp0s8
10.3.0.0/26 dev enp0s8 proto kernel scope link src 10.3.0.13

[dona@johnny ~]$ sudo nmcli device show enp0s8
[...]
IP4.ADDRESS[1]:                         10.3.0.13/26
IP4.GATEWAY:                            10.3.0.62
IP4.DNS[1]:                             10.3.0.253
[...]
```

# **Entracte**
**A ce stade vous avez :**

- **Un routeur qui permet aux machines d'acheminer leur trafic entre les réseaux :+1:**
    - **Entre les LANs :+1:**
    - **Vers internet :+1:**
    > **Vérifié avec `ping 8.8.8.8` + `ping` entre les VM**


- **Un DHCP qui filent des infos à vos clients :+1:**
    - **Une IP, une route par défaut, l'adresse d'un DNS :+1:**
    > **Vérifié avec `sudo nmcli device show enp0s8`**

- **Un DNS :+1:**
    - **Qui résout tous les noms localement :+1:**
    > **Vérifié avec `dig` et `nslookup`** 

# **III. Services métier**

## **1. Serveur Web**
> **🖥️ VM `web1.server2.tp3`**

#### **🌞 Setup d'une nouvelle machine, qui sera un serveur Web, une belle appli pour nos clients**

    ```
    [dona@nfs1 ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
    TYPE=Ethernet
    BOOTPROTO=static
    IPADDR=10.3.1.3
    NETMASK=255.255.255.240
    DEFROUTE=yes
    NAME=enp0s8
    UUID=c077c523-fb65-4a2a-8998-c40b1466e030
    DEVICE=enp0s8
    ONBOOT=yes
    GATEWAY=10.3.1.14
    DNS1=10.3.0.253
    
    [dona@nfs1 ~]$ ip r s
    default via 10.3.1.14 dev enp0s8 proto static metric 100
    10.3.1.0/28 dev enp0s8 proto kernel scope link src 10.3.1.3 metric 100

    [dona@web1 ~]$ sudo nmcli device show enp0s8
    IP4.ADDRESS[1]:                         10.3.1.2/28
    IP4.GATEWAY:                            10.3.1.14
    IP4.DNS[1]:                             10.3.0.253
    
    
    [dona@web1 ~]$ cat /etc/resolv.conf
    # Generated by NetworkManager
    search server2.tp3
    nameserver 10.3.0.253
    
    [dona@web1 ~]$ sudo cat /etc/sysconfig/network
    # Created by anaconda
    GATEWAY=10.3.1.14
    ```
    
    ```
    [dona@web1 ~]$ sudo hostname
    web1.server2.tp3
    ```

    ```
    # Création du serveur python
    [dona@web1 ~]$ sudo vim /etc/systemd/system/web.service
    [dona@web1 ~]$ cat /etc/systemd/system/web.service
    [Unit]
    Description=Very simple web service

    [Service]
    ExecStart=/bin/python3 -m http.server 8888

    [Install]
    WantedBy=multi-user.target
    
    # On relance la conf systemd (pour que le fichier web.service soit découvert)
    [dona@web1 ~]$ sudo systemctl daemon-reload

    # Lancement + Lancement au démarrage du service web
    [dona@web1 ~]$ sudo systemctl start web; sudo systemctl enable web
    Created symlink /etc/systemd/system/multi-user.target.wants/web.service → /etc/systemd/system/web.service.
    ```
    
    - **Dans tous les cas, n'oubliez pas d'ouvrir le port associé dans le firewall pour que le serveur web soit joignable**
    ```
    # Ouverture du port 8888/tcp défini dans /etc/systemd/system/web.service
    
    [dona@web1 ~]$ sudo firewall-cmd --add-port=8888/tcp --permanent; sudo firewall-cmd --reload; sudo firewall-cmd --list-all
    success
    success
    public (active)
      target: default
      icmp-block-inversion: no
      interfaces: enp0s8
      sources:
      services: ssh
      ports: 8888/tcp
      protocols:
      masquerade: no
      forward-ports:
      source-ports:
      icmp-blocks:
      rich rules:
    ```

#### **🌞 Test test test et re-test**

    ```
    [dona@dns1 ~]$ sudo nano /var/named/server2.tp3.forward
    [sudo] password for dona:
    [dona@dns1 ~]$ sudo cat /var/named/server2.tp3.forward | grep web1
    web1    IN A    10.3.1.2
    
    [dona@marcel ~]$ curl web1.server2.tp3:8888
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
    <html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Directory listing for /</title>
    </head>
    <body>
    <h1>Directory listing for /</h1>
    <hr>
    <ul>
    <li><a href="bin/">bin@</a></li>
    <li><a href="boot/">boot/</a></li>
    <li><a href="dev/">dev/</a></li>
    <li><a href="etc/">etc/</a></li>
    <li><a href="home/">home/</a></li>
    <li><a href="lib/">lib@</a></li>
    <li><a href="lib64/">lib64@</a></li>
    <li><a href="media/">media/</a></li>
    <li><a href="mnt/">mnt/</a></li>
    <li><a href="opt/">opt/</a></li>
    <li><a href="proc/">proc/</a></li>
    <li><a href="root/">root/</a></li>
    <li><a href="run/">run/</a></li>
    <li><a href="sbin/">sbin@</a></li>
    <li><a href="srv/">srv/</a></li>
    <li><a href="sys/">sys/</a></li>
    <li><a href="tmp/">tmp/</a></li>
    <li><a href="usr/">usr/</a></li>
    <li><a href="var/">var/</a></li>
    </ul>
    <hr>
    </body>
    </html>
    ```

## **2. Partage de fichiers**
### **A. L'introduction wola**
### **B. Le setup wola**
> **🖥️ VM `nfs1.server2.tp3`**
#### **🌞 Setup d'une nouvelle machine, qui sera un serveur NFS**

    ```
    [dona@nfs1 ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
    TYPE=Ethernet
    BOOTPROTO=static
    IPADDR=10.3.1.3
    NETMASK=255.255.255.240
    DEFROUTE=yes
    NAME=enp0s8
    UUID=c077c523-fb65-4a2a-8998-c40b1466e030
    DEVICE=enp0s8
    ONBOOT=yes
    GATEWAY=10.3.1.14
    DNS1=10.3.0.253
    
    [dona@nfs1 ~]$ sudo nmcli device show enp0s8
    [...]
    IP4.ADDRESS[1]:                         10.3.1.3/28
    IP4.GATEWAY:                            10.3.1.14
    IP4.DNS[1]:                             10.3.0.253
    [...]
    ```
    ```
    [dona@nfs1 ~]$ hostname
    `nfs1.server2.tp3`
    
    [dona@dns1 ~]$ sudo cat /var/named/server2.tp3.forward | grep nfs1
    nfs1    IN A    10.3.1.3
    ```
    ```
    [dona@nfs1 ~]$ sudo dnf install -y nfs-utils
    [dona@nfs1 ~]$ sudo nano /etc/idmapd.conf
    [dona@nfs1 ~]$ sudo cat /etc/idmapd.conf | grep Domain
    Domain = server2.tp3

    [dona@nfs1 ~]$ sudo mkdir /srv/nfs_share/
    [dona@nfs1 ~]$ sudo nano /etc/exports
    [dona@nfs1 ~]$ sudo cat /etc/exports
    /srv/nfs_share 10.3.1.0/28(rw,no_root_squash)
    
    # Autorisation NFS dans le pare-feu
    [dona@nfs1 ~]$ sudo firewall-cmd --add-service=nfs --permanent; sudo firewall-cmd --reload; sudo firewall-cmd --list-all
    success
    success
    public (active)
      target: default
      icmp-block-inversion: no
      interfaces: enp0s8
      sources:
      services: nfs ssh
      ports:
      protocols:
      masquerade: no
      forward-ports:
      source-ports:
      icmp-blocks:
      rich rules:
      
      [dona@nfs1 ~]$ systemctl enable --now rpcbind nfs-server
    ```
#### **🌞 Configuration du client NFS**

    ```
    [dona@web1 ~]$ sudo dnf -y install nfs-utils
    [dona@web1 ~]$ sudo nano /etc/idmapd.conf
    [dona@web1 ~]$ sudo cat /etc/idmapd.conf | grep Domain
    Domain = server2.tp3    
    ```
    ```
    [dona@web1 ~]$ sudo mount -t nfs nfs1.server2.tp3:/srv/nfs_share /srv/nfs
    [dona@web1 ~]$ df -hT
    Filesystem                      Type      Size  Used Avail Use% Mounted on
    devtmpfs                        devtmpfs  387M     0  387M   0% /dev
    tmpfs                           tmpfs     405M     0  405M   0% /dev/shm
    tmpfs                           tmpfs     405M  5.6M  400M   2% /run
    tmpfs                           tmpfs     405M     0  405M   0% /sys/fs/cgroup
    /dev/mapper/rl-root             xfs       6.2G  2.2G  4.1G  35% /
    /dev/sda1                       xfs      1014M  301M  714M  30% /boot
    tmpfs                           tmpfs      81M     0   81M   0% /run/user/1000
    nfs1.server2.tp3:/srv/nfs_share nfs4      6.2G  2.2G  4.1G  35% /srv/nfs
    ```

#### **🌞 TEEEEST**

    ```
    [dona@web1 ~]$ cd /srv/nfs
    [dona@web1 nfs]$ sudo mkdir Dossier1
    [dona@web1 nfs]$ sudo touch MonserveurNFS.text

    [dona@web1 nfs]$ sudo cat MonserveurNFS.text
    J'écris du texte depuis web1.server2.tp3, qui est dans le réseau 10.3.1.0/28 que j'ai précisé dans le fichier idmapd.conf sur ma VM nfs1.server2.tp3

    [dona@web1 nfs]$ ls -al
    total 4
    drwxr-xr-x. 3 root root  48 Sep 30 06:17 .
    drwxr-xr-x. 3 root root  17 Sep 30 05:55 ..
    drwxr-xr-x. 2 root root   6 Sep 30 06:17 Dossier1
    -rw-r--r--. 1 root root 152 Sep 30 06:20 MonserveurNFS.text
    ```

    ```
    [dona@nfs1 ~]$ cd /srv/nfs_share/
    [dona@nfs1 nfs_share]$ ls -al
    total 4
    drwxr-xr-x. 3 root root  48 Sep 30 06:17 .
    drwxr-xr-x. 3 root root  23 Sep 30 05:20 ..
    drwxr-xr-x. 2 root root   6 Sep 30 06:17 Dossier1
    -rw-r--r--. 1 root root 152 Sep 30 06:20 MonserveurNFS.text

    [dona@nfs1 nfs_share]$ cat MonserveurNFS.text
    J'écris du texte depuis web1.server2.tp3, qui est dans le réseau 10.3.1.0/28 que j'ai précisé dans le fichier idmapd.conf sur ma VM nfs1.server2.tp3
    ```





