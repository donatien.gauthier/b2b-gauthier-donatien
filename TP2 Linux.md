# TP2 pt. 1 : Gestion de service

# I. Un premier serveur web

## 1. Installation

🌞 **Installer le serveur Apache**

```sudo dnf install httpd```
```sudo cat /etc/httpd/conf/httpd.conf```
```sudo NANO /etc/httpd/conf/httpd.conf```


🌞 **Démarrer le service Apache**

```
[dona@web ~]$ sudo systemctl start httpd
[dona@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
[dona@web ~]$ sudo ss -alntp
[sudo] password for dona:
State      Recv-Q     Send-Q         Local Address:Port         Peer Address:Port    Process
LISTEN     0          128                  0.0.0.0:22                0.0.0.0:*        users:(("sshd",pid=842,fd=5))
LISTEN     0          128                     [::]:22                   [::]:*        users:(("sshd",pid=842,fd=7))
LISTEN     0          128                        *:80                      *:*        users:(("httpd",pid=2038,fd=4),("httpd",pid=2037,fd=4),("httpd",pid=2036,fd=4),("httpd",pid=2034,fd=4))
[dona@web ~]$
[dona@web ~]$
[dona@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
```

🌞 **TEST**

```
[dona@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-09-29 16:52:02 CEST; 1min 20s ago
     Docs: man:httpd.service(8)
 Main PID: 2330 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 5798)
   Memory: 24.2M
   CGroup: /system.slice/httpd.service
           ├─2330 /usr/sbin/httpd -DFOREGROUND
           ├─2331 /usr/sbin/httpd -DFOREGROUND
           ├─2332 /usr/sbin/httpd -DFOREGROUND
           ├─2333 /usr/sbin/httpd -DFOREGROUND
           └─2334 /usr/sbin/httpd -DFOREGROUND

Sep 29 16:52:02 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Sep 29 16:52:02 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Sep 29 16:52:02 web.tp2.linux httpd[2330]: Server configured, listening on: port 80
```
- vérifier avec une commande `curl localhost` que vous joignez votre serveur web localement
- vérifier avec votre navigateur (sur votre PC) que vous accéder à votre serveur web

## 2. Avancer vers la maîtrise du service

🌞 **Le service Apache...**

```enable```
```
[dona@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-09-29 16:52:02 CEST; 1min 20s ago
     Docs: man:httpd.service(8)
 Main PID: 2330 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 5798)
   Memory: 24.2M
   CGroup: /system.slice/httpd.service
           ├─2330 /usr/sbin/httpd -DFOREGROUND
           ├─2331 /usr/sbin/httpd -DFOREGROUND
           ├─2332 /usr/sbin/httpd -DFOREGROUND
           ├─2333 /usr/sbin/httpd -DFOREGROUND
           └─2334 /usr/sbin/httpd -DFOREGROUND

Sep 29 16:52:02 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Sep 29 16:52:02 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Sep 29 16:52:02 web.tp2.linux httpd[2330]: Server configured, listening on: port 80
```
``` 
[dona@web ~]$ sudo find /etc -name httpd.service
/etc/systemd/system/multi-user.target.wants/httpd.service
[dona@web ~]$ sudo cat /etc/systemd/system/multi-user.target.wants/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#       [Service]
#       Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

🌞 **Déterminer sous quel utilisateur tourne le processus Apache**

```
[dona@web ~]$ sudo cat /etc/httpd/conf/httpd.conf

ServerRoot "/etc/httpd"

Listen 80

Include conf.modules.d/*.conf

User apache
Group apache
```
```
[dona@web ~]$ ps -ef | grep httpd
root        2330       1  0 16:52 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2331    2330  0 16:52 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2332    2330  0 16:52 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2333    2330  0 16:52 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2334    2330  0 16:52 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
dona        2667    2000  0 17:33 pts/0    00:00:00 grep --color=auto httpd
```

🌞 **Changer l'utilisateur utilisé par Apache**

```
[dona@web ~]$ sudo useradd apacheuser -m -d /usr/share/httpd -s /sbin/nologin
[dona@web ~]$ sudo vim /etc/httpd/conf/httpd.conf

user : apacheuser
groups : apacheuser

```
```
[dona@web ~]$ sudo systemctl restart httpd
[dona@web ~]$ ps -ef
root        1760       1  0 14:38 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apacheu+    1762    1760  0 14:38 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apacheu+    1763    1760  0 14:38 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apacheu+    1764    1760  0 14:38 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apacheu+    1765    1760  0 14:38 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
dona        1977    1662  0 14:38 pts/0    00:00:00 ps -ef
```

🌞 **Faites en sorte que Apache tourne sur un autre port**

```
[dona@web ~]$ sudo firewall-cmd --add-port=81/tcp --permanent
success
[dona@web ~]$ sudo vim /etc/httpd/conf/httpd.conf

Listen : 81
```
```
[dona@web ~]$ sudo ss -alnpt
State           Recv-Q          Send-Q                   Local Address:Port                   Peer Address:Port         Process
LISTEN          0               128                                  *:81                                *:*             users:(("httpd",pid=2001,fd=4),("httpd",pid=2000,fd=4),("httpd",pid=1999,fd=4),("httpd",pid=1996,fd=4))
```
```
[dona@web ~]$ curl httpd.conf
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>301 Moved Permanently</title>
</head><body>
<h1>Moved Permanently</h1>
<p>The document has moved <a href="http://www.ynov.com/">here</a>.</p>
</body></html>
```

📁 **Fichier `/etc/httpd/conf/httpd.conf`** (que vous pouvez renommer si besoin, vu que c'est le même nom que le dernier fichier demandé)

# II. Une stack web plus avancée

## 1. Intro

## 2. Setup

🖥️ **VM db.tp2.linux**

| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 80          |               |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | 3306        |               |



### A. Serveur Web et NextCloud


🌞 Install du serveur Web et de NextCloud sur `web.tp2.linux`
```
 44  sudo dnf install epel-release
   45  sudo dnf update
   46  sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
   47  sudo dnf module list php
   48  sudo dnf module enable php:remi-7.4
   49  sudo dnf module list php
   50  sudo php remi-7.4 [e] common [d], devel, minimal PHP scripting language
   51  sudo dnf install httpd vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
   52  sudo systemctl enable httpd
   54  mkdir -p /etc/httpd/sites-available/linux.tp2.web
   55  sudo mkdir -p /etc/httpd/sites-available/linux.tp2.web
   64  sudo vim /etc/httpd/sites-available/linux.tp2.web
   77  sudo ln -s /etc/httpd/sites-available/linux.tp2.web /etc/httpd/sites-enabled
   78  sudo mkdir -p /var/www/sub-domains/linux.tp2.web/html
   79  sudo cd /usr/share/zoneinfo
   80  sudo vim /etc/opt/remi/php74/php.ini
   81  clear
   82  ls -al /etc/localtime
   89  sudo cat /etc/httpd/sites-available/linux.tp2.web
   91  sudo wget https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
   92  sudo unzip nextcloud-21.0.1.zip
   93  cd nextcloud
   94  cp -Rf * /var/www/sub-domains/linux.tp2.web/html/
   95  sudo cp -Rf * /var/www/sub-domains/linux.tp2.web/html/
```
```
[dona@web ~]$ sudo cat /etc/httpd/sites-available/linux.tp2.web
<VirtualHost *:80>
  DocumentRoot /var/www/sub-domains/linux.tp2.web/html/
  ServerName  web.tp2.linux

  <Directory /var/www/sub-domains/linux.tp2.web/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```
```
[dona@web ~]$sudo cat /etc/httpd/conf/httpd.conf 
IncludeOptional conf.d/*.conf
IncludeOptional sites-enabled/*
```

### B. Base de données

🌞 **Install de MariaDB sur `db.tp2.linux`**

```
    7  sudo dnf install mariadb-server
    8  sudo systemctl enable mariadb
    9  sudo systemctl start mariadb
   10  mysql_secure_installation
   11  hystory
```
```
[dona@db ~]$ ss -alntp
State   Recv-Q  Send-Q   Local Address:Port     Peer Address:Port  Process
LISTEN  0       128            0.0.0.0:22            0.0.0.0:*
LISTEN  0       80                   *:3306                *:*
LISTEN  0       128               [::]:22               [::]:*
```

🌞 **Préparation de la base pour NextCloud**

```
[dona@db ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 19
Server version: 10.3.28-MariaDB MariaDB Server

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.001 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]>
```

🌞 **Exploration de la base de données**

```
[dona@web ~]$ sudo mysql -u nextcloud -h 10.102.1.12 -p
Enter password:

mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.00 sec)

mysql> USE <DATABASE_NAME>;
ERROR 1044 (42000): Access denied for user 'nextcloud'@'10.102.1.11' to database '<DATABASE_NAME>'
mysql> USE nextcloud;
Database changed
mysql> SHOW TABLES;
Empty set (0.00 sec)
```
```
MariaDB [(none)]> SELECT user, host, plugin, authentication_string FROM mysql.user;
+-----------+-------------+--------+-----------------------+
| user      | host        | plugin | authentication_string |
+-----------+-------------+--------+-----------------------+
| root      | localhost   |        |                       |
| root      | 127.0.0.1   |        |                       |
| root      | ::1         |        |                       |
| nextcloud | 10.102.1.11 |        |                       |
+-----------+-------------+--------+-----------------------+
4 rows in set (0.000 sec)

```

### C. Finaliser l'installation de NextCloud

🌞 sur votre PC

![](/image/nextcloud.png)

🌞 **Exploration de la base de données**

```
MariaDB [(none)]> SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = 'nextcloud';
+----------+
| COUNT(*) |
+----------+
|       77 |
+----------+
1 row in set (0.001 sec)
```