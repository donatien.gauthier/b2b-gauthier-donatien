# TP1 Linux - B2B

## Préparation des VM

#### Configuration réseau

On définit les IP de la carte Host-Only avec : 
`sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8`

REMPLACER dans ce fichier :
`BOOTPROTO=dhcp` => `BOOTPROTO=static`
`ONBOOT=no` => `ONBOOT=yes`

On rajoute ces 2 lignes sous **BOOTPROTO**: 
```
IPADDR=10.10.1.11 (1ère VM) ||| 10.10.1.12 (2ème VM)
NETMASK=255.255.255.0
```

#### Changement du nom de la machine

On tape `sudo nano /etc/hostname` puis on met les noms indiqués dans le tableau (`node1.tp1.b2`, `node2.tp1.b2`)

`sudo reboot now` => vérifier la prise en compte avec la commande `hostname`.

#### Changer le DNS des VM

`sudo nano /etc/resolv.conf` => On enlève les lignes nameserver et on remplace par **nameserver 1.1.1.1**

`dig ynov.com` => ![](/image/screendig.png)

#### Ping entre les 2 machines

`sudo nano /etc/hosts` => On rajoute : 
1ère machine => `10.10.1.12 node2.tp1.b2`
2ème machine => `10.10.1.11 node1.tp1.b2`
On peut maintenant faire `ping node2.tp1.b2` sur la 1ère machine ou `ping node1.tp1.b2` sur la 2ème.


