# TP4 : Vers un réseau d'entreprise

On va utiliser GNS3 dans ce TP pour se rapprocher d'un cas réel. On va focus sur l'aspect routing/switching, avec du matériel Cisco. On va aussi mettre en place des VLANs.


# I. Dumb switch

## 1. Topologie 1


## 2. Adressage topologie 1

| Node  | IP            |
|-------|---------------|
| `pc1` | `10.1.1.1/24` |
| `pc2` | `10.1.1.2/24` |

## 3. Setup topologie 1

🌞 **Commençons simple**

```
pc2> ip 10.1.1.2/24
Checking for duplicate address...
pc1 : 10.1.1.2 255.255.255.0

pc2> save
Saving startup configuration to startup.vpc
.  done

pc2> ping 10.1.1.1
84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=21.316 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=25.155 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=30.684 ms
84 bytes from 10.1.1.1 icmp_seq=4 ttl=64 time=16.058 ms
```

> Jusque là, ça devrait aller. Noter qu'on a fait aucune conf sur le switch. Tant qu'on ne fait rien, c'est une bête multiprise.

# II. VLAN

**Le but dans cette partie va être de tester un peu les *VLANs*.**

On va rajouter **un troisième client** qui, bien que dans le même réseau, sera **isolé des autres grâce aux *VLANs***.

**Les *VLANs* sont une configuration à effectuer sur les *switches*.** C'est les *switches* qui effectuent le blocage.

Le principe est simple :

- déclaration du VLAN sur tous les switches
  - un VLAN a forcément un ID (un entier)
  - bonne pratique, on lui met un nom
- sur chaque switch, on définit le VLAN associé à chaque port
  - genre "sur le port 35, c'est un client du VLAN 20 qui est branché"


## 1. Topologie 2


## 2. Adressage topologie 2

| Node  | IP            | VLAN |
|-------|---------------|------|
| `pc1` | `10.1.1.1/24` | 10   |
| `pc2` | `10.1.1.2/24` | 10   |
| `pc3` | `10.1.1.3/24` | 20   |

### 3. Setup topologie 2

🌞 **Adressage**

```
pc3> show ip

NAME        : pc3[1]
IP/MASK     : 10.1.1.3/24
GATEWAY     : 0.0.0.0
DNS         :
MAC         : 00:50:79:66:68:02
LPORT       : 10007
RHOST:PORT  : 127.0.0.1:10008
MTU:        : 1500

pc3> ping 10.1.1.2
84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=22.518 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=36.780 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=27.457 ms
```

🌞 **Configuration des VLANs**

```
Switch<> en
Switch# conf t
Switch(config)# vlan 10
Switch(config-vlan)# name admins
Switch(config-vlan)# exit
Switch(config)# vlan 20
Switch(config-vlan)# name guests
Switch(config-vlan)# exit
Switch(config)# exit

Switch#
Switch#show vlan

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi0/0, Gi0/1, Gi0/2, Gi0/3
                                                Gi1/0, Gi1/1, Gi1/2, Gi1/3
                                                Gi2/0, Gi2/1, Gi2/2, Gi2/3
                                                Gi3/0, Gi3/1, Gi3/2, Gi3/3
10   admins                           active
20   guests                           active
1002 fddi-default                     act/unsup
1003 token-ring-default               act/unsup
1004 fddinet-default                  act/unsup
1005 trnet-default                    act/unsup

VLAN Type  SAID       MTU   Parent RingNo BridgeNo Stp  BrdgMode Trans1 Trans2
---- ----- ---------- ----- ------ ------ -------- ---- -------- ------ ------
1    enet  100001     1500  -      -      -        -    -        0      0   10   enet  100010     1500  -      -      -        -    -        0      0   20   enet  100020     1500  -      -      -        -    -        0      0   1002 fddi  101002     1500  -      -      -        -    -        0      0   1003 tr    101003     1500  -      -      -        -    -        0      0   1004 fdnet 101004     1500  -      -      -        ieee -        0      0   1005 trnet 101005     1500  -      -      -        ibm  -        0      0


Primary Secondary Type              Ports
------- --------- ----------------- ------------------------------------------

```
```
Switch#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
Switch(config)#
Switch(config)#interface fastEthernet0/0
                          ^
% Invalid input detected at '^' marker.

Switch(config)#
Switch(config)#interface GI0/0
Switch(config-if)#
Switch(config-if)#switchport mod access
Switch(config-if)#
Switch(config-if)#switchport access vlan 10
Switch(config-if)#
Switch(config-if)#exit
Switch(config)#interface GI0/1
Switch(config-if)#
Switch(config-if)#switchport mod access
Switch(config-if)#
Switch(config-if)#switchport access vlan 10
Switch(config-if)#
Switch(config-if)#exit
Switch(config)#
Switch(config)#interface GI0/2
Switch(config-if)#
Switch(config-if)#switchport mod access
Switch(config-if)#
Switch(config-if)#switchport access vlan 20
Switch(config-if)#
Switch(config-if)#exit
```

🌞 **Vérif**

```
pc1> ping 10.1.1.2
84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=21.257 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=47.658 ms

pc1> ping 10.1.1.3
host (10.1.1.3) not reachable
```

# III. Routing

Dans cette partie, on va donner un peu de sens aux VLANs :

- un pour les serveurs du réseau
  - on simulera ça avec un p'tit serveur web
- un pour les admins du réseau
- un pour les autres random clients du réseau

Cela dit, il faut que tout ce beau monde puisse se ping, au moins joindre le réseau des serveurs, pour accéder au super site-web.

**Bien que bloqué au niveau du switch à cause des VLANs, le trafic pourra passer d'un VLAN à l'autre grâce à un routeur.**

Il assurera son job de routeur traditionnel : router entre deux réseaux. Sauf qu'en plus, il gérera le changement de VLAN à la volée.

## 1. Topologie 3


## 2. Adressage topologie 3

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
|-----------|---------------|--------------|
| `clients` | `10.1.1.0/24` | 11           |
| `admins`  | `10.2.2.0/24` | 12           |
| `servers` | `10.3.3.0/24` | 13           |

L'adresse des machines au sein de ces réseaux :

| Node               | `clients`       | `admins`        | `servers`       |
|--------------------|-----------------|-----------------|-----------------|
| `pc1.clients.tp4`  | `10.1.1.1/24`   | x               | x               |
| `pc2.clients.tp4`  | `10.1.1.2/24`   | x               | x               |
| `adm1.admins.tp4`  | x               | `10.2.2.1/24`   | x               |
| `web1.servers.tp4` | x               | x               | `10.3.3.1/24`   |
| `r1`               | `10.1.1.254/24` | `10.2.2.254/24` | `10.3.3.254/24` |

## 3. Setup topologie 3

🖥️ VM `web1.servers.tp4`, déroulez la [Checklist VM Linux](#checklist-vm-linux) dessus

🌞 **Adressage**

- définissez les IPs statiques sur toutes les machines **sauf le *routeur***

🌞 **Configuration des VLANs**

```
[...]
Switch(config)#vlan 13
Switch(config-vlan)#
Switch(config-vlan)#name servers
Switch(config-vlan)#
Switch(config-vlan)#exit
Switch(config)#
Switch(config)#interface GI0/0
Switch(config-if)#
Switch(config-if)#switchport mode access
Switch(config-if)#
Switch(config-if)#switchport access vlan 11
Switch(config-if)#
Switch(config-if)#exit
Switch(config)#
Switch(config)#interface GI0/1
Switch(config-if)#
Switch(config-if)#switchport mode access
Switch(config-if)#
Switch(config-if)#switchport access vlan 11
Switch(config-if)#
Switch(config-if)#exit
Switch(config)#
Switch(config)#interface
Switch(config)#interface GI0/2
Switch(config-if)#
Switch(config-if)#switchport mode access
Switch(config-if)#
Switch(config-if)#switchport access vlan 12
Switch(config-if)#
Switch(config-if)#exit
Switch#show vlan br

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi0/3, Gi1/1, Gi1/2, Gi1/3
                                                Gi2/0, Gi2/1, Gi2/2, Gi2/3
                                                Gi3/0, Gi3/1, Gi3/2, Gi3/3
11   clients                          active    Gi0/0, Gi0/1
12   admins                           active    Gi0/2
13   servers                          active    Gi1/0
```
```
Switch(config)#interface GI0/3
Switch(config-if)#
Switch(config-if)#switchport trunk encapsulation dot1q
Switch(config-if)#
Switch(config-if)#switchport mode trunk
Switch(config-if)#
Switch(config-if)#switchport trunk allowed vlan add 11,12,13
Switch(config-if)#
Switch(config-if)#exit
Switch(config)#
Switch(config)#exit
Switch#
Switch#
*Oct 21 13:33:19.768: %SYS-5-CONFIG_I: Configured from console by console
Switch#
Switch#show interface trunk

Port        Mode             Encapsulation  Status        Native vlan
Gi0/3       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Gi0/3       1-4094

Port        Vlans allowed and active in management domain
Gi0/3       1,11-13,20

Port        Vlans in spanning tree forwarding state and not pruned
Gi0/3       1,11-13,20
```

➜ **Pour le *routeur***

- ici, on va avoir besoin d'un truc très courant pour un *routeur* : qu'il porte plusieurs IP sur une unique interface
  - avec Cisco, on crée des "sous-interfaces" sur une interface
  - et on attribue une IP à chacune de ces sous-interfaces
- en plus de ça, il faudra l'informer que, pour chaque interface, elle doit être dans un VLAN spécifique

Pour ce faire, un exemple. On attribue deux IPs `192.168.1.254/24` VLAN 11 et `192.168.2.254` VLAN12 à un *routeur*. L'interface concernée sur le *routeur* est `fastEthernet 0/0` :

```cisco
# conf t

(config)# interface fastEthernet 0/0.10
R1(config-subif)# encapsulation dot1Q 10
R1(config-subif)# ip addr 192.168.1.254 255.255.255.0 
R1(config-subif)# exit

(config)# interface fastEthernet 0/0.20
R1(config-subif)# encapsulation dot1Q 20
R1(config-subif)# ip addr 192.168.2.254 255.255.255.0 
R1(config-subif)# exit
```

🌞 **Config du *routeur***
```
R1#sh ip int br
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            unassigned      YES unset  up                    up
FastEthernet0/0.10         unassigned      YES unset  deleted               down
FastEthernet0/0.11         10.1.1.254      YES manual up                    up
FastEthernet0/0.12         10.2.2.254      YES manual up                    up
FastEthernet0/0.13         10.3.3.254      YES manual up                    up
```

🌞 **Vérif**

```
pc1> ping 10.1.1.254
84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=24.374 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=27.826 ms
```
```
pc2> ip 10.1.1.2/24 10.1.1.254
Checking for duplicate address...
pc1 : 10.1.1.2 255.255.255.0 gateway 10.1.1.254

pc2> show ip

NAME        : pc2[1]
IP/MASK     : 10.1.1.2/24
GATEWAY     : 10.1.1.254
DNS         :
MAC         : 00:50:79:66:68:01
LPORT       : 10004
RHOST:PORT  : 127.0.0.1:10005
MTU:        : 1500
```
  - testez des `ping` entre les réseaux

# IV. NAT

On va ajouter une fonctionnalité au routeur : le NAT.

On va le connecter à internet (simulation du fait d'avoir une IP publique) et il va faire du NAT pour permettre à toutes les machines du réseau d'avoir un accès internet.

## 1. Topologie 4


## 2. Adressage topologie 4

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
|-----------|---------------|--------------|
| `clients` | `10.1.1.0/24` | 11           |
| `servers` | `10.2.2.0/24` | 12           |
| `routers` | `10.3.3.0/24` | 13           |

L'adresse des machines au sein de ces réseaux :

| Node               | `clients`       | `admins`        | `servers`       |
|--------------------|-----------------|-----------------|-----------------|
| `pc1.clients.tp4`  | `10.1.1.1/24`   | x               | x               |
| `pc2.clients.tp4`  | `10.1.1.2/24`   | x               | x               |
| `adm1.admins.tp4`  | x               | `10.2.2.1/24`   | x               |
| `web1.servers.tp4` | x               | x               | `10.3.3.1/24`   |
| `r1`               | `10.1.1.254/24` | `10.2.2.254/24` | `10.3.3.254/24` |

## 3. Setup topologie 4

🌞 **Ajoutez le noeud Cloud à la topo**

```
R1#conf t
R1(config)#interface FastEthernet1/0
R1(config-if)#ip address dhcp
R1(config-if)#no shut
R1(config-if)#exit
R1(config)#do show ip int br
    
Interface                  IP-Address      OK? Method Status                Protocol
[...]
FastEthernet1/0            10.0.3.16       YES DHCP   up                    up
[....]
```
```
R1#ping 1.1.1.1

Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 1.1.1.1, timeout is 2 seconds:
.!!!!
Success rate is 80 percent (4/5), round-trip min/avg/max = 64/66/68 ms

pc1> ping 1.1.1.1 -c 3

1.1.1.1 icmp_seq=1 timeout
84 bytes from 1.1.1.1 icmp_seq=2 ttl=62 time=34.337 ms
84 bytes from 1.1.1.1 icmp_seq=3 ttl=62 time=28.270 ms
```

🌞 **Configurez le NAT**

```
R1(config)#interface FastEthernet1/0
R1(config-if)#ip nat outside
R1(config-if)#exit
R1(config)#interface FastEthernet0/0
R1(config-if)#ip nat inside
R1(config-if)#exit
R1(config)#access-list 1 permit any
R1(config)#ip nat inside source list 1 interface fastEthernet 1/0 overload
R1(config)#ip domain lookup
R1(config)#do ping google.com

Translating "google.com"...domain server (192.168.122.1) [OK]

Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 216.58.213.78, timeout is 2 seconds:
!!!!!
Success rate is 100 percent (5/5), round-trip min/avg/max = 24/108/164 ms
```

🌞 **Test**

```
pc1> ip dns 8.8.8.8
pc1> show ip

NAME        : pc1[1]
IP/MASK     : 10.1.1.1/24
GATEWAY     : 10.1.1.254
DNS         : 8.8.8.8
```
```
pc2> ip dns 8.8.8.8
pc2> show ip

NAME        : pc2[1]
IP/MASK     : 10.1.1.2/24
GATEWAY     : 10.1.1.254
DNS         : 8.8.8.8
```
```
adm1> ip dns 8.8.8.8
adm1> show ip

NAME        : adm1[1]
IP/MASK     : 10.2.2.1/24
GATEWAY     : 10.2.2.254
DNS         : 8.8.8.8
```
```
[dona@web1 ~]$ sudo cat /etc/resolv.conf
# Generated by NetworkManager
search lan servers.tp4
nameserver 192.168.122.254
nameserver 8.8.8.8   
```
```
[dona@web1 ~]$ ping google.com -c 4
PING google.com (172.217.18.206) 56(84) bytes of data.
64 bytes from ham02s14-in-f206.1e100.net (172.217.18.206): icmp_seq=1 ttl=113 time=47.1 ms
64 bytes from ham02s14-in-f206.1e100.net (172.217.18.206): icmp_seq=2 ttl=113 time=68.5 ms
64 bytes from ham02s14-in-f206.1e100.net (172.217.18.206): icmp_seq=3 ttl=113 time=50.1 ms
64 bytes from ham02s14-in-f206.1e100.net (172.217.18.206): icmp_seq=4 ttl=113 time=61.1 ms

--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 7114ms
rtt min/avg/max/mdev = 47.117/56.697/68.506/8.580 ms
```
```
pc1> ping google.com -c 3
google.com resolved to 142.250.201.174

84 bytes from 142.250.201.174 icmp_seq=1 ttl=114 time=47.394 ms
84 bytes from 142.250.201.174 icmp_seq=2 ttl=114 time=113.020 ms
84 bytes from 142.250.201.174 icmp_seq=3 ttl=114 time=44.584 ms
```
```
adm1> ping google.com -c 3
google.com resolved to 142.250.75.238

84 bytes from 142.250.75.238 icmp_seq=1 ttl=114 time=69.196 ms
84 bytes from 142.250.75.238 icmp_seq=2 ttl=114 time=57.925 ms
84 bytes from 142.250.75.238 icmp_seq=3 ttl=114 time=89.070 ms
```

# V. Add a building

On a acheté un nouveau bâtiment, faut tirer et configurer un nouveau switch jusque là-bas.

On va en profiter pour setup un serveur DHCP pour les clients qui s'y trouvent.

## 1. Topologie 5


## 2. Adressage topologie 5

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
|-----------|---------------|--------------|
| `clients` | `10.1.1.0/24` | 11           |
| `servers` | `10.2.2.0/24` | 12           |
| `routers` | `10.3.3.0/24` | 13           |

L'adresse des machines au sein de ces réseaux :

| Node                | `clients`       | `admins`        | `servers`       |
|---------------------|-----------------|-----------------|-----------------|
| `pc1.clients.tp4`   | `10.1.1.1/24`   | x               | x               |
| `pc2.clients.tp4`   | `10.1.1.2/24`   | x               | x               |
| `pc3.clients.tp4`   | DHCP            | x               | x               |
| `pc4.clients.tp4`   | DHCP            | x               | x               |
| `pc5.clients.tp4`   | DHCP            | x               | x               |
| `dhcp1.clients.tp4` | `10.1.1.253/24` | x               | x               |
| `adm1.admins.tp4`   | x               | `10.2.2.1/24`   | x               |
| `web1.servers.tp4`  | x               | x               | `10.3.3.1/24`   |
| `r1`                | `10.1.1.254/24` | `10.2.2.254/24` | `10.3.3.254/24` |

## 3. Setup topologie 5

Vous pouvez partir de la topologie 4. 

🌞  **Vous devez me rendre le `show running-config` de tous les équipements**

- de tous les équipements réseau
  - le routeur
  - les 3 switches

> N'oubliez pas les VLANs sur tous les switches.

🌞  **Mettre en place un serveur DHCP dans le nouveau bâtiment**

- il doit distribuer des IPs aux clients dans le réseau `clients` qui sont branchés au même switch que lui
- sans aucune action manuelle, les clients doivent...
  - avoir une IP dans le réseau `clients`
  - avoir un accès au réseau `servers`
  - avoir un accès WAN
  - avoir de la résolution DNS

> Réutiliser les serveurs DHCP qu'on a monté dans les autres TPs.

🌞  **Vérification**

- un client récupère une IP en DHCP
- il peut ping le serveur Web
- il peut ping `8.8.8.8`
- il peut ping `google.com`

> Faites ça sur n'importe quel VPCS que vous venez d'ajouter : `pc3` ou `pc4` ou `pc5`.


