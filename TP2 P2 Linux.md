# TP2 pt. 2 : Maintien en condition opérationnelle

# I. Monitoring

On bouge pas pour le moment niveau machines :

| Machine         | IP            | Service                 | Port ouvert | IPs autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | ?           | ?             |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | ?           | ?             |

## 1. Le concept

La surveillance ou *monitoring* consiste à surveiller la bonne santé d'une entité.  
J'utilise volontairement le terme vague "entité" car cela peut être très divers :

- une machine
- une application
- un lien entre deux machines
- etc.

---

**Le monitoring s'effectue en plusieurs étapes :**

- ***scraping***
  - un programme tourne sur la machine pour récupérer des métriques sur le système
  - récupérer l'état de remplissage de la RAM par exemple
- ***centralisation*** des données (optionnel)
  - dans le cas d'un gros parc de machines, les métriques récupérées sont centralisées sur un unique serveur
- ***visualisation*** des données
  - une joulie interface est dispo pour visualiser les métriques
  - des courbes dans tous les sens
- ***alerting***
  - l'administrateur définit des seuils critiques pour certaines métriques
  - si le seuil est dépassé, une alerte est envoyée (mail, Discord, Slack, etc.) pour être prévenu immédiatement d'un soucis
  - dans le cas de la RAM par exemple, on sera prévenus **avant** que la RAM soit remplie

---

**Dans notre cas on va surveiller deux choses :**

- d'une part, les machines : ***monitoring système***. Par exemple :
  - remplissage disque/RAM
  - charge CPU/réseau
- d'autre part, nos applications : ***monitoring applicatif***. Ici :
  - serveur Web
  - base de données

## 2. Setup

🌞 **Manipulation du *service* Netdata**
```
   41  sudo su -
       bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
   42  sudo systemctl status netdata
   43  sudo systemctl enable netdata
```
```
[dona@web ~]$ sudo ss -alntp | grep net
LISTEN 0      128          0.0.0.0:19999      0.0.0.0:*    users:(("netdata",pid=2686,fd=5))

LISTEN 0      128        127.0.0.1:8125       0.0.0.0:*    users:(("netdata",pid=2686,fd=43))

LISTEN 0      128             [::]:19999         [::]:*    users:(("netdata",pid=2686,fd=6))

LISTEN 0      128            [::1]:8125          [::]:*    users:(("netdata",pid=2686,fd=32))
[dona@web ~]$ sudo firewall-cmd --add-port=8125/tcp --permanent
[dona@web ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
```

**Eeeeet.... c'est tout !**, rendez-vous sur `http://IP_VM:PORT` pour accéder à l'interface Web de Netdata (depuis un navigateur sur votre PC).  
**C'est sexy na ? Et c'est en temps réel :3**

🌞 **Setup Alerting**
```
[dona@web ~]$ sudo vim /opt/netdata/etc/netdata.conf
###############################################################################
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL=https://discord.com/api/webhooks/897110878871224441/GTKKawd1C79f9owp4qbjj_lawXlMwkN24qoDpZflrg06GuTibIRcUhdWTrsqvNkPKoxQ

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"
```
```
[dona@web ~]$ grep sysadmin /opt/netdata/etc/netdata/health_alarm_notify.conf
[dona@web ~]$ sudo su -s /bin/bash netdata
bash-4.4$ export NETDATA_ALARM_NOTIFY_DEBUG=1
bash-4.4$ /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test
```

# II. Backup

| Machine            | IP            | Service                 | Port ouvert | IPs autorisées |
|--------------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | ?           | ?             |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | ?           | ?             |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | ?           | ?             |

## 1. Intwo bwo

**La *backup* consiste à extraire des données de leur emplacement original afin de les stocker dans un endroit dédié.**  

**Cet endroit dédié est un endroit sûr** : le but est d'assurer la perennité des données sauvegardées, tout en maintenant leur niveau de sécurité.

Pour la sauvegarde, il existe plusieurs façon de procéder. Pour notre part, nous allons procéder comme suit :

- **création d'un serveur de stockage**
  - il hébergera les sauvegardes de tout le monde
  - ce sera notre "endroit sûr"
  - ce sera un partage NFS
  - ainsi, toutes les machines qui en ont besoin pourront accéder à un dossier qui leur est dédié sur ce serveur de stockage, afin d'y stocker leurs sauvegardes
- **développement d'un script de backup**
  - ce script s'exécutera en local sur les machines à sauvegarder
  - il s'exécute à intervalles de temps réguliers
  - il envoie les données à sauvegarder sur le serveur NFS
  - du point de vue du script, c'est un dossier local. Mais en réalité, ce dossier est monté en NFS.

![You're supposed to backup everything](./pics/backup_meme.jpg)

## 2. Partage NFS

🌞 **Setup environnement**

```
[dona@backup ~]$ sudo mkdir /srv/backup/
[dona@backup ~]$ sudo mkdir /srv/backup/web.tp2.linux/
```

🌞 **Setup partage NFS**

```
[dona@backup ~]$ sudo dnf -y install nfs-utils
[dona@backup ~]$ sudo vim /etc/idmapd.conf
[dona@backup ~]$ sudo cat /etc/idmapd.conf | grep Domain
Domain = tp2.linux
[dona@backup ~]$ sudo vim /etc/exports
[dona@backup ~]$ sudo cat /etc/exports
/srv/backup/web.tp2.linux 10.102.1.11/24(rw,no_root_squash)
[dona@backup ~]$ sudo systemctl enable --now rpcbind nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
```

🌞 **Setup points de montage sur `web.tp2.linux`**

```
[dona@web ~]$ sudo mkdir /srv/backup/
[dona@web ~]$ sudo dnf -y install nfs-utils
[dona@web ~]$ sudo vim /etc/idmapd.conf
[dona@web ~]$ sudo vim /etc/hosts
[dona@web ~]$ sudo mount -t nfs backup.tp2.linux:/srv/backup/web.tp2.linux /srv/backup
[dona@web ~]$ sudo mount | grep backup
backup.tp2.linux:/srv/backup/web.tp2.linux on /srv/backup type nfs4 (rw,relatime,vers=4.2,rsize=131072,wsize=131072,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,clientaddr=10.102.1.11,local_lock=none,addr=10.102.1.13)
[dona@web ~]$ sudo df -h | grep backup
backup.tp2.linux:/srv/backup/web.tp2.linux  6.2G  4.0G  2.3G  65% /srv/backup
[dona@web ~]$ sudo touch /srv/backup/test.txt
[dona@web ~]$ sudo ls -l /srv/backup/
total 0
-rw-r--r--. 1 root root 0 Oct 11 17:18 test.txt
```
```
[dona@web ~]$ sudo cat /etc/fstab | grep backup
backup.tp2.linux:/srv/backup/web.tp2.linux /srv/backup nfs defaults 0 0
```

🌟 **BONUS** : partitionnement avec LVM

```
[dona@backup ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0    8G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0    7G  0 part
  ├─rl-root 253:0    0  6.2G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
sdb           8:16   0    8G  0 disk
sr0          11:0    1 1024M  0 rom
[dona@backup ~]$ sudo pvcreate /dev/sdb
[sudo] password for dona:
  Physical volume "/dev/sdb" successfully created.
[dona@backup ~]$ sudo pvs
  PV         VG Fmt  Attr PSize  PFree
  /dev/sda2  rl lvm2 a--  <7.00g    0
  /dev/sdb      lvm2 ---   8.00g 8.00g
  [dona@backup ~]$ sudo vgcreate group /dev/sdb
  Volume group "group" successfully created
[dona@backup ~]$ sudo bgs
sudo: bgs: command not found
[dona@backup ~]$ sudo vgs
  VG    #PV #LV #SN Attr   VSize  VFree
  group   1   0   0 wz--n- <8.00g <8.00g
  rl      1   2   0 wz--n- <7.00g     0
[dona@backup ~]$ sudo lvcreate -L 5G group -n lv5Go
  Logical volume "lv5Go" created.
[dona@backup ~]$ sudo mkfs -t ext4 /dev/mapper/group-lv5Go
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 1310720 4k blocks and 327680 inodes
Filesystem UUID: 9d4ce4b5-7009-4a70-a6f4-7bbdf76b6906
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912, 819200, 884736

Allocating group tables: done
Writing inode tables: done
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done
[dona@backup ~]$ sudo pvs
  PV         VG    Fmt  Attr PSize  PFree
  /dev/sda2  rl    lvm2 a--  <7.00g     0
  /dev/sdb   group lvm2 a--  <8.00g <3.00g
[dona@backup ~]$ sudo mount /dev/group/lv5Go /srv/backup
[dona@backup ~]$ sudo pvs
  PV         VG    Fmt  Attr PSize  PFree
  /dev/sda2  rl    lvm2 a--  <7.00g     0
  /dev/sdb   group lvm2 a--  <8.00g <3.00g
[dona@backup ~]$ sudo vgs
  VG    #PV #LV #SN Attr   VSize  VFree
  group   1   1   0 wz--n- <8.00g <3.00g
  rl      1   2   0 wz--n- <7.00g     0
[dona@backup ~]$ df -h
Filesystem               Size  Used Avail Use% Mounted on
devtmpfs                 455M     0  455M   0% /dev
tmpfs                    473M  340K  472M   1% /dev/shm
tmpfs                    473M  6.5M  466M   2% /run
tmpfs                    473M     0  473M   0% /sys/fs/cgroup
/dev/mapper/rl-root      6.2G  4.0G  2.3G  64% /
/dev/sda1               1014M  241M  774M  24% /boot
tmpfs                     95M     0   95M   0% /run/user/1000
/dev/mapper/group-lv5Go  4.9G   20M  4.6G   1% /srv/backup
[dona@backup ~]$ sudo cat /etc/fstab | grep lv5Go
backup.tp2.linux:/dev/mapper/group-lv5Go /srv/backup nfs defaults 0 0
```

## 3. Backup de fichiers

**Un peu de scripting `bash` !** Le scripting est le meilleur ami de l'admin, vous allez pas y couper hihi.  

La syntaxe de `bash` est TRES particulière, mais ce que je vous demande de réaliser là est un script minimaliste.

Votre script **DEVRA**...

- comporter un shebang
- comporter un commentaire en en-tête qui indique le but du script, en quelques mots
- comporter un commentaire qui indique l'auteur et la date d'écriture du script

Par exemple :

```bash
#!/bin/bash
# Simple backup script
# it4 - 09/10/2021

...
```

🌞 **Rédiger le script de backup `/srv/tp2_backup.sh`**
🌞 **Tester le bon fonctionnement**

```
[dona@backup srv]$ sudo cat /srv/tp2_backup.sh
#!/bin/bash
# Simple backup script
# Donatien - 12/10/2021

DESTINATION=$1
DIR_TO_BK=$2


if [ -z "$DESTINATION" ]; then

  echo "Warning : Please provide the folder name as an argument"
  exit 0
fi


if [ -d "$DESTINATION" ]; then
        filename="${DIR_TO_BK}_$(date '+%d-%m-%y-%h-%m-%s').tar.gz"
        tar -cpzf "$filename" "$DIR_TO_BK"
        rsync -av $filename $DESTINATION
        echo "Archive successfully created."
        else
        echo "WARNING: Directory name doesn't exists: $1"

fi
[dona@backup srv]$ sudo bash -x ./tp2_backup.sh /srv/backup tp2_backup
+ DESTINATION=/srv/backup
+ DIR_TO_BK=tp2_backup
+ '[' -z /srv/backup ']'
+ '[' -d /srv/backup ']'
++ date +%d-%m-%y-%h-%m-%s
+ filename=tp2_backup_12-10-21-Oct-10-1634050281.tar.gz
+ tar -cpzf tp2_backup_12-10-21-Oct-10-1634050281.tar.gz tp2_backup
tar: tp2_backup: Cannot stat: No such file or directory
tar: Exiting with failure status due to previous errors
+ rsync -av tp2_backup_12-10-21-Oct-10-1634050281.tar.gz /srv/backup
sending incremental file list
tp2_backup_12-10-21-Oct-10-1634050281.tar.gz

sent 174 bytes  received 35 bytes  418.00 bytes/sec
total size is 45  speedup is 0.22
+ echo 'Archive successfully created.'
Archive successfully created.
[dona@backup srv]$ ls -al
total 32
drwxr-xr-x.  4 root root 4096 Oct 12 16:51 .
dr-xr-xr-x. 19 root root  283 Oct 12 16:48 ..
drwxr-xr-x.  4 root root  137 Oct 12 16:51 backup
drwxr-xr-x.  2 root root    6 Oct 12 15:57 test
-rw-r--r--.  1 root root   45 Oct 12 16:51 tp2_backup_12-10-21-Oct-10-1634050281.tar.gz
-rwxr-xr-x.  1 root root  473 Oct 12 16:48 tp2_backup.sh
[dona@backup srv]$ cd backup/
[dona@backup backup]$ ls -al
total 12
drwxr-xr-x. 4 root root  137 Oct 12 16:51 .
drwxr-xr-x. 4 root root 4096 Oct 12 16:51 ..
drwxr-xr-x. 2 root root    6 Oct 12 15:57 test
-rw-r--r--. 1 root root   45 Oct 12 16:51 tp2_backup_12-10-21-Oct-10-1634050281.tar.gz
drwxr-xr-x. 2 root root   22 Oct 11 17:18 web.tp2.linux
```

## 4. Unité de service

Lancer le script à la main c'est bien. **Le mettre dans une joulie *unité de service* et l'exécuter à intervalles réguliers, de manière automatisée, c'est mieux.**

Le but va être de créer un *service* systemd pour que vous puissiez interagir avec votre script de sauvegarde en faisant :

```bash
$ sudo systemctl start tp2_backup
$ sudo systemctl status tp2_backup
```

Ensuite on créera un *timer systemd* qui permettra de déclencher le lancement de ce *service* à intervalles réguliers.

### A. Unité de service

🌞 **Créer une *unité de service*** pour notre backup
🌞 **Tester le bon fonctionnement**

```
[dona@backup ~]$ sudo vim /etc/systemd/system/tp2_backup.service
[dona@backup ~]$ sudo cat /etc/systemd/system/tp2_backup.service
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/srv/tp2_backup.sh /srv/backup /srv/test
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target

[dona@backup ~]$ sudo systemctl daemon-reload
[dona@backup ~]$ sudo systemctl start tp2_backup
[dona@backup ~]$ sudo systemctl status tp2_backup
● tp2_backup.service - Our own lil backup service (TP2)
   Loaded: loaded (/etc/systemd/system/tp2_backup.service; disabled; vendor preset: disabled)
   Active: inactive (dead)

Oct 13 14:40:01 backup.tp2.linux systemd[1]: Starting Our own lil backup service (TP2)...
Oct 13 14:40:01 backup.tp2.linux tp2_backup.sh[2286]: tar: Removing leading `/' from member names
Oct 13 14:40:01 backup.tp2.linux tp2_backup.sh[2286]: sending incremental file list
Oct 13 14:40:01 backup.tp2.linux tp2_backup.sh[2286]: test_13-10-21-Oct-10-1634128801.tar.gz
Oct 13 14:40:01 backup.tp2.linux tp2_backup.sh[2286]: sent 236 bytes  received 35 bytes  542.00 bytes/sec
Oct 13 14:40:01 backup.tp2.linux tp2_backup.sh[2286]: total size is 112  speedup is 0.41
Oct 13 14:40:01 backup.tp2.linux tp2_backup.sh[2286]: Archive successfully created.
Oct 13 14:40:01 backup.tp2.linux systemd[1]: tp2_backup.service: Succeeded.
Oct 13 14:40:01 backup.tp2.linux systemd[1]: Started Our own lil backup service (TP2).
[dona@backup ~]$ cd /srv/backup
[dona@backup backup]$ ls -al
total 12
drwxr-xr-x. 3 root root  125 Oct 13 14:40 .
drwxr-xr-x. 4 root root 4096 Oct 13 14:40 ..
-rw-r--r--. 1 root root  112 Oct 13 14:40 test_13-10-21-Oct-10-1634128801.tar.gz
-rw-r--r--. 1 root root   45 Oct 12 16:51 tp2_backup_12-10-21-Oct-10-1634050281.tar.gz
drwxr-xr-x. 2 root root   22 Oct 11 17:18 web.tp2.linux
```

### B. Timer


```bash
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp2_backup.service

[Timer]
Unit=tp2_backup.service
OnCalendar=*-*-* *:*:00

[Install]
WantedBy=timers.target
```


🌞 **Activez le timer**

```
[dona@backup ~]$ sudo systemctl start tp2_backup.timer
[dona@backup ~]$ sudo systemctl enable tp2_backup.timer
Created symlink /etc/systemd/system/timers.target.wants/tp2_backup.timer → /etc/systemd/system/tp2_backup.timer.
[dona@backup ~]$ sudo systemctl status tp2_backup.timer
● tp2_backup.timer - Periodically run our TP2 backup script
   Loaded: loaded (/etc/systemd/system/tp2_backup.timer; enabled; vendor preset: disabled)
   Active: active (waiting) since Wed 2021-10-13 14:48:10 CEST; 35s ago
  Trigger: Wed 2021-10-13 14:49:00 CEST; 13s left

Oct 13 14:48:10 backup.tp2.linux systemd[1]: Started Periodically run our TP2 backup script.
```

🌞 **Tests !**

```
[dona@backup srv]$ ls -al
total 72
drwxr-xr-x.  4 root root 4096 Oct 13 14:54 .
dr-xr-xr-x. 19 root root 4096 Oct 12 17:10 ..
drwxr-xr-x.  3 root root 4096 Oct 13 14:54 backup
drwxr-xr-x.  2 root root    6 Oct 12 15:57 test
-rw-r--r--.  1 root root  107 Oct 12 16:49 test_12-10-21-Oct-10-1634050169.tar.gz
-rw-r--r--.  1 root root  112 Oct 13 14:40 test_13-10-21-Oct-10-1634128801.tar.gz
-rw-r--r--.  1 root root  112 Oct 13 14:48 test_13-10-21-Oct-10-1634129290.tar.gz
-rw-r--r--.  1 root root  112 Oct 13 14:49 test_13-10-21-Oct-10-1634129343.tar.gz
-rw-r--r--.  1 root root  112 Oct 13 14:50 test_13-10-21-Oct-10-1634129403.tar.gz
-rw-r--r--.  1 root root  112 Oct 13 14:51 test_13-10-21-Oct-10-1634129463.tar.gz
-rw-r--r--.  1 root root  112 Oct 13 14:52 test_13-10-21-Oct-10-1634129523.tar.gz
-rw-r--r--.  1 root root  112 Oct 13 14:53 test_13-10-21-Oct-10-1634129583.tar.gz
-rw-r--r--.  1 root root  112 Oct 13 14:54 test_13-10-21-Oct-10-1634129643.tar.gz
-rw-r--r--.  1 root root   45 Oct 12 16:51 tp2_backup_12-10-21-Oct-10-1634050281.tar.gz
-rwxr-xr-x.  1 root root  473 Oct 12 16:54 tp2_backup.sh
```

### C. Contexte

🌞 **Faites en sorte que...**

```
[dona@web nextcloud]$ sudo mount | grep backup
backup.tp2.linux:/srv/backup/web.tp2.linux on /var/www/sub-domains/web.tp2.linux type nfs4

[dona@backup backup]$ sudo systemctl start tp2_backup
[dona@backup backup]$ ls -l Archives/
total 142312
-rw-r--r--. 1 root root 145725914 Oct 25 00:04 web.tp2.linux_21-10-25_22-20-31.tar.gz
```
```
[dona@backup backup]$ sudo cat /etc/systemd/system/tp2_backup.timer | grep "OnCalendar"
OnCalendar=*-*-* 3:15:00
```

## 5. Backup de base de données

Sauvegarder des dossiers c'est bien. Mais sauvegarder aussi les bases de données c'est mieux.

🌞 **Création d'un script `/srv/tp2_backup_db.sh`**

- il utilise la commande `mysqldump` pour récupérer les données de la base de données
- cela génère un fichier `.sql` qui doit ensuite être compressé en `.tar.gz`
- il s'exécute sur la machine `db.tp2.linux`
- il s'utilise de la façon suivante :

```bash
$ ./tp2_backup_db.sh <DESTINATION> <DATABASE>
```

📁 **Fichier `/srv/tp2_backup_db.sh`**  

🌞 **Restauration**

- tester la restauration de données
- c'est à dire, une fois la sauvegarde effectuée, et le `tar.gz` en votre possession, tester que vous êtes capables de restaurer la base dans l'état au moment de la sauvegarde
  - il faut réinjecter le fichier `.sql` dans la base à l'aide d'une commmande `mysql`

🌞 ***Unité de service***

- pareil que pour la sauvegarde des fichiers ! On va faire de ce script une *unité de service*.
- votre script `/srv/tp2_backup_db.sh` doit pouvoir se lancer grâce à un *service* `tp2_backup_db.service`
- le *service* est exécuté tous les jours à 03h30 grâce au *timer* `tp2_backup_db.timer`
- prouvez le bon fonctionnement du *service* ET du *timer*

📁 **Fichier `/etc/systemd/system/tp2_backup_db.timer`**  
📁 **Fichier `/etc/systemd/system/tp2_backup_db.service`**

## 6. Petit point sur la backup

A ce stade vous avez :

- un script qui tourne sur `web.tp2.linux` et qui **sauvegarde les fichiers de NextCloud**
- un script qui tourne sur `db.tp2.linux` et qui **sauvegarde la base de données de NextCloud**
- toutes **les backups sont centralisées** sur `backup.tp2.linux`
- **tout est géré de façon automatisée**
  - les scripts sont packagés dans des *services*
  - les services sont déclenchés par des *timers*
  - tout est paramétré pour s'allumer quand les machines boot (les *timers* comme le serveur NFS)

🔥🔥 **That is clean shit.** 🔥🔥

# III. Reverse Proxy

## 1. Introooooo

Un *reverse proxy* est un outil qui sert d'intermédiaire entre le client et un serveur donné (souvent un serveur Web).

**C'est l'admin qui le met en place, afin de protéger l'accès au serveur Web.**

Une fois en place, le client devra saisir l'IP (ou le nom) du *reverse proxy* pour accéder à l'application Web (ce ne sera plus directement l'IP du serveur Web).

Un *reverse proxy* peut permettre plusieurs choses :

- chiffrement
  - c'est lui qui mettra le HTTPS en place (protocole HTTP + chiffrement avec le protocole TLS)
  - on pourrait le faire directement avec le serveur Web (Apache) dans notre cas
  - pour de meilleures performances, il est préférable de dédier une machine au chiffrement HTTPS, et de laisser au serveur web un unique job : traiter les requêtes HTTP
- répartition de charge
  - plutôt qu'avoir un seul serveur Web, on peut en setup plusieurs
  - ils hébergent tous la même application
  - le *reverse proxy* enverra les clients sur l'un ou l'autre des serveurs Web, afin de répartir la charge à traiter
- d'autres trucs
  - caching de ressources statiques (CSS, JSS, images, etc.)
  - tolérance de pannes
  - ...

---

**Dans ce TP on va setup un reverse proxy NGINX très simpliste.**

![Apache at the back hihi](./pics/nginx-at-the-front-apache-at-the-back.jpg)

## 2. Setup simple

| Machine            | IP            | Service                 | Port ouvert | IPs autorisées |
|--------------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | ?           | ?             |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | ?           | ?             |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | ?           | ?             |
| `front.tp2.linux`  | `10.102.1.14` | Reverse Proxy           | ?           | ?             |

🖥️ **VM `front.tp2.linu`x**

**Déroulez la [📝**checklist**📝](#checklist) sur cette VM.**

🌞 **Installer NGINX**

- vous devrez d'abord installer le paquet `epel-release` avant d'installer `nginx`
  - EPEL c'est des dépôts additionnels pour Rocky
  - NGINX n'est pas présent dans les dépôts par défaut que connaît Rocky
- le fichier de conf principal de NGINX est `/etc/nginx/nginx.conf`

🌞 **Tester !**

- lancer le *service* `nginx`
- le paramétrer pour qu'il démarre seul quand le système boot
- repérer le port qu'utilise NGINX par défaut, pour l'ouvrir dans le firewall
- vérifier que vous pouvez joindre NGINX avec une commande `curl` depuis votre PC

🌞 **Explorer la conf par défaut de NGINX**

- repérez l'utilisateur qu'utilise NGINX par défaut
- dans la conf NGINX, on utilise le mot-clé `server` pour ajouter un nouveau site
  - repérez le bloc `server {}` dans le fichier de conf principal
- par défaut, le fichier de conf principal inclut d'autres fichiers de conf
  - mettez en évidence ces lignes d'inclusion dans le fichier de conf principal

🌞 **Modifier la conf de NGINX**

- pour que ça fonctionne, le fichier `/etc/hosts` de la machine **DOIT** être rempli correctement, conformément à la **[📝**checklist**📝](#checklist)**
- supprimer le bloc `server {}` par défaut, pour ne plus présenter la page d'accueil NGINX
- créer un fichier `/etc/nginx/conf.d/web.tp2.linux.conf` avec le contenu suivant :
  - j'ai sur-commenté pour vous expliquer les lignes, n'hésitez pas à dégommer mes lignes de commentaires

```bash
[it4@localhost nginx]$ cat conf.d/web.tp2.linux.conf 
server {
    # on demande à NGINX d'écouter sur le port 80 pour notre NextCloud
    listen 80;

    # ici, c'est le nom de domaine utilisé pour joindre l'application
    # ce n'est pas le nom du reverse proxy, mais le nom que les clients devront saisir pour atteindre le site
    server_name web.tp2.linux; # ici, c'est le nom de domaine utilisé pour joindre l'application (pas forcéme

    # on définit un comportement quand la personne visite la racine du site (http://web.tp2.linux/)
    location / {
        # on renvoie tout le trafic vers la machine web.tp2.linux
        proxy_pass http://web.tp2.linux;
    }
}
```

## 3. Bonus HTTPS

**Etape bonus** : mettre en place du chiffrement pour que nos clients accèdent au site de façon plus sécurisée.

🌟 **Générer la clé et le certificat pour le chiffrement**

- il existe plein de façons de faire
- nous allons générer en une commande la clé et le certificat
- puis placer la clé et le cert dans les endroits standards pour la distribution Rocky Linux

```bash
# On se déplace dans un dossier où on peut écrire
$ cd ~

# Génération de la clé et du certificat
# Attention à bien saisir le nom du site pour le "Common Name"
$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt
[...]
Common Name (eg, your name or your server\'s hostname) []:web.tp2.linux
[...]

# On déplace la clé et le certificat dans les dossiers standards sur Rocky
# En le renommant
$ sudo mv server.key /etc/pki/tls/private/web.tp2.linux.key
$ sudo mv server.crt /etc/pki/tls/certs/web.tp2.linux.crt

# Setup des permissions restrictives
$ sudo chown root:root /etc/pki/tls/private/web.tp2.linux.key
$ sudo chown root:root /etc/pki/tls/certs/web.tp2.linux.crt
$ sudo chmod 400 /etc/pki/tls/private/web.tp2.linux.key
$ sudo chmod 644 /etc/pki/tls/certs/web.tp2.linux.crt
```

🌟 **Modifier la conf de NGINX**

- inspirez-vous de ce que vous trouvez sur internet
- il n'y a que deux lignes à ajouter
  - une ligne pour préciser le chemin du certificat
  - une ligne pour préciser le chemin de la clé
- et une ligne à modifier
  - préciser qu'on écoute sur le port 443, avec du chiffrement
- n'oubliez pas d'ouvrir le port 443/tcp dans le firewall

🌟 **TEST**

- connectez-vous sur `https://web.tp2.linux` depuis votre PC
- petite avertissement de sécu : normal, on a signé nous-mêmes le certificat
  - vous pouvez donc "Accepter le risque" (le nom du bouton va changer suivant votre navigateur)
  - avec `curl` il faut ajouter l'option `-k` pour désactiver cette vérification

# IV. Firewalling

**On va rendre nos firewalls un peu plus agressifs.**

Actuellement je vous ai juste demandé d'autoriser le trafic sur tel ou tel port. C'est bien.

**Maintenant on va restreindre le trafic niveau IP aussi.**

Par exemple : notre base de données `db.tp2.linux` n'est accédée que par le serveur Web `web.tp2.linux`, et par aucune autre machine.  
On va donc configurer le firewall de la base de données pour qu'elle n'accepte QUE le trafic qui vient du serveur Web.

**On va *harden* ("durcir" en français) la configuration de nos firewalls.**

## 1. Présentation de la syntaxe

> **N'oubliez pas d'ajouter `--permanent` sur toutes les commandes `firewall-cmd`** si vous souhaitez que le changement reste effectif après un rechargement de FirewallD.

**Première étape** : définir comme politique par défaut de TOUT DROP. On refuse tout, et on whiteliste après.

Il existe déjà une zone appelée `drop` qui permet de jeter tous les paquets. Il suffit d'ajouter nos interfaces dans cette zone.

```bash
$ sudo firewall-cmd --list-all # on voit qu'on est par défaut dans la zone "public"
$ sudo firewall-cmd --set-default-zone=drop # on configure la zone "drop" comme zone par défaut
$ sudo firewall-cmd --zone=drop --add-interface=enp0s8 # ajout explicite de l'interface host-only à la zone "drop"
```

**Ensuite**, on peut créer une nouvelle zone, qui autorisera le trafic lié à telle ou telle IP source :

```bash
$ sudo firewall-cmd --add-zone=ssh # le nom "ssh" est complètement arbitraire. C'est clean de faire une zone par service.
```

**Puis** on définit les règles visant à autoriser un trafic donné :

```bash
$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 # 10.102.1.1 sera l'IP autorisée
$ sudo firewall-cmd --zone=ssh --add-port=22/tcp # uniquement le trafic qui vient 10.102.1.1, à destination du port 22/tcp, sera autorisé
```

**Le comportement de FirewallD sera alors le suivant :**

- si l'IP source d'un paquet est `10.102.1.1`, il traitera le paquet comme étant dans la zone `ssh`
- si l'IP source est une autre IP, et que le paquet arrive par l'interface `enp0s8` alors le paquet sera géré par la zone `drop` (le paquet sera donc *dropped* et ne sera jamais traité)

> *L'utilisation de la notation `IP/32` permet de cibler une IP spécifique. Si on met le vrai masque `10.102.1.1/24` par exemple, on autorise TOUT le réseau `10.102.1.0/24`, et non pas un seul hôte. Ce `/32` c'est un truc qu'on voit souvent en réseau, pour faire référence à une IP unique.*

![Cut here to activate firewall :D](./pics/cut-here-to-activate-firewall-best-label-for-lan-cable.jpg)

## 2. Mise en place

### A. Base de données

🌞 **Restreindre l'accès à la base de données `db.tp2.linux`**

- seul le serveur Web doit pouvoir joindre la base de données sur le port 3306/tcp
- vous devez aussi autoriser votre accès SSH
- n'hésitez pas à multiplier les zones (une zone `ssh` et une zone `db` par exemple)

> Quand vous faites une connexion SSH, vous la faites sur l'interface Host-Only des VMs. Cette interface est branchée à un Switch qui porte le nom du Host-Only. Pour rappel, votre PC a aussi une interface branchée à ce Switch Host-Only.  
C'est depuis cette IP que la VM voit votre connexion. C'est cette IP que vous devez autoriser dans le firewall de votre VM pour SSH.

🌞 **Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`**

- `sudo firewall-cmd --get-active-zones`
- `sudo firewall-cmd --get-default-zone`
- `sudo firewall-cmd --list-all --zone=?`

### B. Serveur Web

🌞 **Restreindre l'accès au serveur Web `web.tp2.linux`**

- seul le reverse proxy `front.tp2.linux` doit accéder au serveur web sur le port 80
- n'oubliez pas votre accès SSH

🌞 **Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`**

### C. Serveur de backup

🌞 **Restreindre l'accès au serveur de backup `backup.tp2.linux`**

- seules les machines qui effectuent des backups doivent être autorisées à contacter le serveur de backup *via* NFS
- n'oubliez pas votre accès SSH

🌞 **Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`**

### D. Reverse Proxy

🌞 **Restreindre l'accès au reverse proxy `front.tp2.linux`**

- seules les machines du réseau `10.102.1.0/24` doivent pouvoir joindre le proxy
- n'oubliez pas votre accès SSH

🌞 **Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`**

### E. Tableau récap

🌞 **Rendez-moi le tableau suivant, correctement rempli :**

| Machine            | IP            | Service                 | Port ouvert | IPs autorisées |
|--------------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | ?           | ?             |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | ?           | ?             |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | ?           | ?             |
| `front.tp2.linux`  | `10.102.1.14` | Reverse Proxy           | ?           | ?             |